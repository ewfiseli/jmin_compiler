

import sys

config.src_root = "/home/eric/other-workspace/jmin_compiler"
config.lit_tools_dir = "/home/eric/other-workspace/jmin_compiler/third-party/llvm/utils/lit"
config.python_executable = "/usr/local/bin/python"


# Let the main config do the real work.
lit_config.load_config(config, "/home/eric/other-workspace/jmin_compiler/tests/lit.cfg.py")
