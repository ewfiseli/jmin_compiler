// RUN: %jmin %s --output=- --no-library --no-prologue | FileCheck %s

// CHECK-LABEL: @START MAIN main
// CHECK-NEXT: LMAIN:
// CHECK-NEXT: STMFD sp!, {fp,lr}
// CHECK-NEXT: SUB sp, #0
// CHECK-NEXT: MOV fp, sp
// CHECK-NEXT: L0:
// CHECK-NEXT: MOV sp, fp
// CHECK-NEXT: ADD sp, #0
// CHECK-NEXT LDMFD sp!, {fp, pc}
// CHECK: @END MAIN
main() {

}
