/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef GLOBALSEMANTICVISITOR_H
#define GLOBALSEMANTICVISITOR_H

#include "compiler/compiler.h"
#include "visitors/nodevisitor.h"

using namespace jmin;

class SymbolTable;

class GlobalSemanticVisitor : public NodeVisitor {
public:
  GlobalSemanticVisitor(Context &Ctx) : Ctx(Ctx) {}

  virtual void Visit(AST::Node *node);
  virtual void Visit(AST::AdditiveExpression *node);
  virtual void Visit(AST::ArgumentList *node);
  virtual void Visit(AST::Assignment *node);
  virtual void Visit(AST::Block *node);
  virtual void Visit(AST::BlockStatement *node);
  virtual void Visit(AST::BlockStatements *node);
  virtual void Visit(AST::BreakStatement *node);
  virtual void Visit(AST::ConditionalAndExpression *node);
  virtual void Visit(AST::ConditionalOrExpression *node);
  virtual void Visit(AST::EqualityExpression *node);
  virtual void Visit(AST::FormalParam *node);
  virtual void Visit(AST::FormalParamList *node);
  virtual void Visit(AST::FunctionDeclarator *node);
  virtual void Visit(AST::FunctionDecl *node);
  virtual void Visit(AST::FunctionHeader *node);
  virtual void Visit(AST::FunctionInvocation *node);
  virtual void Visit(AST::Ident *node);
  virtual void Visit(AST::IfElseStatement *node);
  virtual void Visit(AST::IfStatement *node);
  virtual void Visit(AST::Literal *node);
  virtual void Visit(AST::MainFunctionDeclarator *node);
  virtual void Visit(AST::MainFunctionDecl *node);
  virtual void Visit(AST::MultiplicativeExpression *node);
  virtual void Visit(AST::NullStmt *node);
  virtual void Visit(AST::RelationalExpression *node);
  virtual void Visit(AST::ReturnStatement *node);
  virtual void Visit(AST::RootNode *node);
  virtual void Visit(AST::Type *node);
  virtual void Visit(AST::UnaryExpression *node);
  virtual void Visit(AST::VariableDecl *node);
  virtual void Visit(AST::WhileStatement *node);
  virtual void Visit(AST::DeclRefExpr *node);

  Context &Ctx;
};

#endif /* GLOBALSEMANTICVISITOR_H */
