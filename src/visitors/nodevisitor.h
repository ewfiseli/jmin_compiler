/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef NODEVISITOR_H
#define NODEVISITOR_H

/* Forward declarations for all NodeVisitor types */
namespace AST {
class Node;
class AdditiveExpression;
class ArgumentList;
class Assignment;
class Block;
class BlockStatement;
class BlockStatements;
class BreakStatement;
class ConditionalAndExpression;
class ConditionalOrExpression;
class EqualityExpression;
class FormalParam;
class FormalParamList;
class FunctionDeclarator;
class FunctionDecl;
class FunctionHeader;
class FunctionInvocation;
class Ident;
class IfElseStatement;
class IfStatement;
class Literal;
class MainFunctionDeclarator;
class MainFunctionDecl;
class MultiplicativeExpression;
class NullStmt;
class RelationalExpression;
class ReturnStatement;
class RootNode;
class Primary;
class Type;
class UnaryExpression;
class VariableDecl;
class WhileStatement;
class DeclRefExpr;
} /* namespace AST */

class NodeVisitor {
public:
  virtual ~NodeVisitor(){};
  /* Base case */
  virtual void Visit(AST::Node *node) = 0;
  /* The rest */
  virtual void Visit(AST::AdditiveExpression *node) = 0;
  virtual void Visit(AST::ArgumentList *node) = 0;
  virtual void Visit(AST::Assignment *node) = 0;
  virtual void Visit(AST::Block *node) = 0;
  virtual void Visit(AST::BlockStatement *node) = 0;
  virtual void Visit(AST::BlockStatements *node) = 0;
  virtual void Visit(AST::BreakStatement *node) = 0;
  virtual void Visit(AST::ConditionalAndExpression *node) = 0;
  virtual void Visit(AST::ConditionalOrExpression *node) = 0;
  virtual void Visit(AST::EqualityExpression *node) = 0;
  virtual void Visit(AST::FormalParam *node) = 0;
  virtual void Visit(AST::FormalParamList *node) = 0;
  virtual void Visit(AST::FunctionDeclarator *node) = 0;
  virtual void Visit(AST::FunctionDecl *node) = 0;
  virtual void Visit(AST::FunctionHeader *node) = 0;
  virtual void Visit(AST::FunctionInvocation *node) = 0;
  virtual void Visit(AST::Ident *node) = 0;
  virtual void Visit(AST::IfElseStatement *node) = 0;
  virtual void Visit(AST::IfStatement *node) = 0;
  virtual void Visit(AST::Literal *node) = 0;
  virtual void Visit(AST::MainFunctionDeclarator *node) = 0;
  virtual void Visit(AST::MainFunctionDecl *node) = 0;
  virtual void Visit(AST::MultiplicativeExpression *node) = 0;
  virtual void Visit(AST::NullStmt *node) = 0;
  virtual void Visit(AST::RelationalExpression *node) = 0;
  virtual void Visit(AST::ReturnStatement *node) = 0;
  virtual void Visit(AST::RootNode *node) = 0;
  virtual void Visit(AST::Type *node) = 0;
  virtual void Visit(AST::UnaryExpression *node) = 0;
  virtual void Visit(AST::VariableDecl *node) = 0;
  virtual void Visit(AST::WhileStatement *node) = 0;
  virtual void Visit(AST::DeclRefExpr *node) = 0;
};

#endif /* NODEVISITOR_H */
