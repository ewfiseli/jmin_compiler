/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/codegenerationvisitor.h"
#include "AST/AST.h"
#include "compiler/armfactory.h"
#include "compiler/parser.h"
#include "compiler/symboltable.h"
#include "utils/glog.h"

using namespace AST;

/* Author: Eric Fiselier
 * This is an AST visitor that generates code.
 * the code is written to the stringstream 'code_'
 * I like the idea of writing inline assembly so below
 * you will see macros to help fake inline assembly.
 *
 * note to reader:
 * 	many of these comments are meant to help my terrible memory
 *
 * the entry point for this (and all) visitors MUST be the root node.
 *
 * The ABI basics:
 * 		r0-r4: parameter passing
 * 		r5:	All intermediate values are passed in R5
 * 			R5 should always be the result of the last intermediate
 *expression r6-r7: since R5 is always* (*should be) the destination for any
 *calulation R6 and R7 are used as places for the 2nd and 3rd operands. If a
 *visitor uses R6/R7 they must first save it. This design choice allows for the
 *evaluation of arbitrarly complex expression. r8-r10: these are currently
 *unutilized. r11: the frame pointer. since the stack pointer moves during
 * 			 exicution of a function, it cannot be used to address
 *local variables. the frame pointer points to the TOS after the function
 *prologue. as sample prologue would be as follows stmfd sp!, {lr} sub   sp,
 *#num_vars*4 mov 	  r11, sp @SAVE THE FRAME POINTER the epilogue counters
 *this. but also resets the stack pointer to be equal to the frame pointer. I
 *think that that this is probably a redundant operation. the stack pointer
 *should always (by my assumptions) always be equal to the frame pointer during
 *a return. R11 is the frame pointer. that detail has been abstracted to
 *ARMFactory. the rest: they are used as you would imagine.
 *
 * param passing:
 * 		currently only support 5 parameter passing Parameters are
 * 		passed in r0-r4. that is the only time these registers are used.
 * 		I would like to abuse the nature of this project and just
 *implement CDECL (although that would raise some issues with function
 *prologue/epilogue)
 *
 * concerns: As I said, the stack is modified during the evaluation of
 *expression. It is important that a return statement is never exicuted when the
 *			 stack pointer is not at the correct position.
 */

/* a) because I type these a lot.
 * b) see (pseudo) assembly idea below */
#define R0 std::string("r0")
#define R1 std::string("r1")
#define R2 std::string("r2")
#define R3 std::string("r3")
#define R4 std::string("r4")
#define R5 std::string("r5")
#define R6 std::string("r6")
#define R7 std::string("r7")
#define R8 std::string("r8")
#define R9 std::string("r9")
#define R10 std::string("r10")
#define FP std::string("fp")
#define IP std::string("ip")
#define SP std::string("sp")
#define PC std::string("pc")
#define LR std::string("lr")

#define TRUE_VAL std::string("1")
#define FALSE_VAL std::string("0")

/* ALLCAPS assembly macros are ment to represent (pseudo) inline assembly
 * code_) std::stringstream the current represetation of the assembly file.
 * ArmFactory::instruction)
 * 		returns a std::string - formated assembly intruction(s) */
#define PROLOGUE(main) code_ << ArmFactory::Prologue(main)
#define EPILOGUE() code_ << ArmFactory::Epilogue()
#define RTS() code_ << ArmFactory::RunTimeSystem()
#define FUNC_PROLOGUE(x) code_ << ArmFactory::FunctionPrologue(x)
#define FUNC_EPILOGUE(x) code_ << ArmFactory::FunctionEpilogue(x)
#define PUSH(x) code_ << ArmFactory::Push(x)
#define POP(x) code_ << ArmFactory::Pop(x)
#define MOV(x, y) code_ << ArmFactory::Mov(x, y)
#define ADD(d, x, y) code_ << ArmFactory::Add(d, x, y)
#define SUB(d, x, y) code_ << ArmFactory::Sub(d, x, y)
#define SMUL(d, x, y) code_ << ArmFactory::SMul(d, x, y)
#define SDIV(d, x, y) code_ << ArmFactory::SDiv(d, x, y)
#define MOD(d, x, y) code_ << ArmFactory::Mod(d, x, y)
#define NEG(d, x) code_ << ArmFactory::Negate(d, x)
#define AND(d, x, y) code_ << ArmFactory::And(d, x, y)
#define OR(d, x, y) code_ << ArmFactory::Or(d, x, y)
#define GE(d, x, y) code_ << ArmFactory::GreaterEqual(d, x, y)
#define GT(d, x, y) code_ << ArmFactory::Greater(d, x, y)
#define LE(d, x, y) code_ << ArmFactory::LessEqual(d, x, y)
#define LT(d, x, y) code_ << ArmFactory::Less(d, x, y)
#define EQ(d, x, y) code_ << ArmFactory::Equal(d, x, y)
#define NE(d, x, y) code_ << ArmFactory::NotEqual(d, x, y)
#define STRL(d, x) code_ << ArmFactory::StoreL(d, x)
#define LDR(d, x) code_ << ArmFactory::Load(d, x)
#define ADRL(d, x) code_ << ArmFactory::AddressLong(d, x)
#define CMP(x, y) code_ << ArmFactory::Compare(x, y);
#define B(x) code_ << ArmFactory::BranchAlways(x)
#define BL(x) code_ << ArmFactory::BranchLink(x)
#define BGT(x) code_ << ArmFactory::BranchConditional(std::string("GT"), x)
#define BGE(x) code_ << ArmFactory::BranchConditional(std::string("GE"), x)
#define BLT(x) code_ << ArmFactory::BranchConditional(std::string("LT"), x)
#define BLE(x) code_ << ArmFactory::BranchConditional(std::string("LE"), x)
#define BEQ(x) code_ << ArmFactory::BranchConditional(std::string("EQ"), x)
#define BNE(x) code_ << ArmFactory::BranchConditional(std::string("NE"), x)
#define ASCIZ(s) code_ << ArmFactory::ASCIZString(s);
/* this is cheating, but is is beautiful cheating.
 * None of the strings should contain newlines */
#define OUTPUT_LABEL(s) code_ << s << ":" << std::endl
#define DIRECTIVE(s) code_ << s << std::endl
#define COMMENT(s) code_ << "@" << s << std::endl
#define RAW(s) code_ << s << std::endl

/* Ok, these ones are cheating. I went a little nuts with the
 * inline assembly idea.
 * VAR(s) s) std::string A global label or local ident.
 * 			if local ident:
 * 				return the offset of the id. formated as [fp,
 * #offset] if global: return formated string =label where labels is the global
 * label for the variable
 *
 * LIT(s) s) std::string a string to append "=" to.
 * 		     's' must be in literal pool. 's' may be a string label
 * 			 or a constant integer string.
 *
 * CONST(s) s) a std::string the result of CONST is "#" + s
 * the stringify might cause some problems
 */
#define VAR(s) _GetVarString(s)
#define CONST(i) "#" #i
#define LIT(s) "=" + s

using namespace AST;

CodeGenerationVisitor::CodeGenerationVisitor(Context &xCtx)
    : Ctx(xCtx), local_size_(0), global_size_(0) {}

/* return a string representing the contents of the
 * stringstream  (code) */
std::string CodeGenerationVisitor::ToString() { return code_.str(); }

std::string CodeGenerationVisitor::_GetVarString(std::string id) {
  /* if local return that first  as fp offset */
  if (local_var_offset_.count(id) == 1)
    return ArmFactory::FormatRegisterOffset(FP, local_var_offset_[id]);
  else if (global_var_offset_.count(id) == 1)
    return ArmFactory::FormatRegisterOffset(IP, global_var_offset_[id]);
  else
    FATAL_ERROR();
  FATAL_ERROR();
  return std::string("I SHOULD NOT BE HERE");
}

void CodeGenerationVisitor::_BuildGlobalSection() {
  OUTPUT_LABEL("globals");
  assert(global_size_ % 4 == 0); // assuming word alligned size
  for (int i = 0; i < global_size_ / 4; ++i) {
    DIRECTIVE(".word 0");
  }
}

void CodeGenerationVisitor::_InitGlobalTablesAndSize() {
  global_size_ = 0;
  /* var_table_t = std::map<std::string, AST::VariableDecl*> */
  var_table_t &gtable = Ctx.Symbols->GetGlobalTable();
  for (auto it = gtable.begin(); it != gtable.end(); ++it) {
    global_var_offset_[it->first] = global_size_;
    global_size_ += 4;
  }

  /* Init function labels */
  func_table_t &ftable = Ctx.Symbols->GetFunctionTable();
  for (auto it = ftable.begin(); it != ftable.end(); ++it) {
    function_labels_[it->first] = ArmFactory::GetLabel();
  }
  /* Ok, now fix library refrences */
  function_labels_[std::string("halt")] = "Lhalt";
  function_labels_[std::string("getchar")] = "Lgetchar";
  function_labels_[std::string("printc")] = "Lprintc";
  function_labels_[std::string("printb")] = "Lprintb";
  function_labels_[std::string("printi")] = "Lprinti";
  function_labels_[std::string("prints")] = "Lprints";

  /* special initalization for main */
  main_label_ = std::string("LMAIN");
}

void CodeGenerationVisitor::_InitLocalTablesAndSize(std::string func_name) {
  local_size_ = 0;
  local_var_offset_.clear();
  local_strings_.clear();
  Ctx.Symbols->UnloadLocalTable(); // probably redundant.
  Ctx.Symbols->LoadLocalTable(func_name);
  assert(Ctx.Symbols->LocalTableLoaded());

  var_table_t &ltable = Ctx.Symbols->GetLocalTable();
  for (auto it = ltable.begin(); it != ltable.end(); ++it) {
    local_var_offset_[it->first] = local_size_;
    local_size_ += 4;
  }
}

void CodeGenerationVisitor::_BuildLocalPoolStrings() {
  for (auto it = local_strings_.begin(); it != local_strings_.end(); ++it) {
    OUTPUT_LABEL(it->second);
    ASCIZ(it->first);
  }
}

/* I'M IMPORTANT
 * This is (see: should be, not enforced) the entry point for this visitor
 * other behavior is undefined.
 *
 * Initialize (not outputing) labels for:
 * 	a) library function labels
 * 	b) function labels in the symbol table
 * 	c) global var labels and size of :global data section
 * 	d) main_label_
 * responsible for:
 * 	generating prologue and linking main.
 * 	making recursive visit calls to global nodes
 * 	generating global section
 * 	generating epilogue.
 *  (hopefully) returning gracefully.
 */
void CodeGenerationVisitor::Visit(RootNode *node) {
  /* build the global label table, and record the size needed
   * it global_size_ */
  _InitGlobalTablesAndSize();
  COMMENT("Compiled by Jmin");
  if (Ctx.Opts.IncludePrologue) {
    PROLOGUE(main_label_);
  }
  RAW(""); // insert a blank line

  /* make the recursive visits */
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetNode(i);
    child->AcceptVisit(this);
  }
  if (Ctx.Opts.IncludeLibrary) {
    RAW("");
    COMMENT("BEGIN RTS");
    RTS();
    COMMENT("END RTS");
  }
  /* output the global section, then them epilogue*/
  RAW("");
  _BuildGlobalSection();
  RAW("");
  EPILOGUE();
}

/* entry point for main
 * responsible for:
 * 		load local offset table from symbol table
 * 		(ie alloc stack space, save var/fp offsets for later use)
 * 		output function label
 * 		function prologue
 * 		outputing end function label
 * 		function epilogue
 * 		output LTORG directive
 * 		output local strings
 * 		output align
 * 		clearing the local offset table before exit
 */
void CodeGenerationVisitor::Visit(MainFunctionDecl *node) {
  std::string func_name = node->GetDeclarator()->GetName();
  _InitLocalTablesAndSize(func_name);
  func_end_label_ = ArmFactory::GetLabel();

  COMMENT("START MAIN " + func_name);
  OUTPUT_LABEL(main_label_);
  FUNC_PROLOGUE(local_size_);

  /* generate code recursivly */
  Node *child = node->GetBlock();
  child->AcceptVisit(this);

  OUTPUT_LABEL(func_end_label_);
  FUNC_EPILOGUE(local_size_);
  DIRECTIVE(".ltorg");
  _BuildLocalPoolStrings();
  DIRECTIVE(".align 4");
  COMMENT("END MAIN");
  /* cleanup */
  local_var_offset_.clear();
  local_strings_.clear();
}

/* entry point for function definitions
 * the job of this function is the same as the main function
 * visitor.
 */
void CodeGenerationVisitor::Visit(FunctionDecl *node) {
  std::string func_name = node->GetHeader()->GetName();
  _InitLocalTablesAndSize(func_name);

  func_end_label_ = ArmFactory::GetLabel();

  COMMENT("START " + func_name);
  OUTPUT_LABEL(function_labels_[func_name]);
  FUNC_PROLOGUE(local_size_);

  /* Move passed reg's onto stack */
  FormalParamList *params = node->GetHeader()->GetDeclarator()->GetParamList();
  std::string pname;
  switch (params->Length()) {
  case 5:
    pname = params->GetParam(4)->GetID()->GetName();
    STRL(R4, VAR(pname));
  case 4:
    pname = params->GetParam(3)->GetID()->GetName();
    STRL(R3, VAR(pname));
  case 3:
    pname = params->GetParam(2)->GetID()->GetName();
    STRL(R2, VAR(pname));
  case 2:
    pname = params->GetParam(1)->GetID()->GetName();
    STRL(R1, VAR(pname));
  case 1:
    pname = params->GetParam(0)->GetID()->GetName();
    STRL(R0, VAR(pname));
  case 0:
    break;
  default:
    FATAL_ERROR();
  }

  /* generate code recursivly */
  Node *child = node->GetBlock();
  child->AcceptVisit(this);

  OUTPUT_LABEL(func_end_label_);
  FUNC_EPILOGUE(local_size_);
  DIRECTIVE(".ltorg");
  _BuildLocalPoolStrings();
  DIRECTIVE(".align 4");
  COMMENT("END " + func_name);
  /* cleanup */
  local_var_offset_.clear();
  local_strings_.clear();
}

/* node is really just a base case for the visitor.
 * we are in the base class of all (actually importand nodes.
 * if we are here. we forgot to implement a a method in the
 * visitor/visitie */
void CodeGenerationVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* while statement format:
 * 	 B loop_gaurd
 * loop_body:
 * 	 @LOOP BODY CODE
 * loop_gaurd:
 *   @ Eval expression into R5
 *   CMP R5, TRUE
 *   BEQ loop_body
 * loop_end: @used by break statements
 *
 * the while statement visitor has to store the old break_label_
 * 	then set break_label_ = loop_end
 *  before this visitor exits it must reset the label break_label_ back
 * 	to its old value (possibly junk) to deal with embeded loops
 */
void CodeGenerationVisitor::Visit(WhileStatement *node) {
  std::string loop_gaurd = ArmFactory::GetLabel();
  std::string loop_end = ArmFactory::GetLabel();
  std::string loop_top = ArmFactory::GetLabel();
  std::string old_break_label = break_label_;
  break_label_ = loop_end;
  Node *child;

  B(loop_gaurd);
  OUTPUT_LABEL(loop_top);

  child = node->GetStatement();
  child->AcceptVisit(this);

  OUTPUT_LABEL(loop_gaurd);
  child = node->GetExpression();
  child->AcceptVisit(this);
  /* true or false now in R5 */
  CMP(R5, CONST(1));
  BEQ(loop_top);
  /* end of loop */
  OUTPUT_LABEL(loop_end);
  break_label_ = old_break_label;
}

/* result should be in R5 */
void CodeGenerationVisitor::Visit(AdditiveExpression *node) {
  PUSH(R7);
  PUSH(R6);

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  PUSH(R5);

  child = node->GetSecond();
  child->AcceptVisit(this);

  MOV(R7, R5);
  POP(R6);

  if (node->GetOperator() == PLUS_OP)
    ADD(R5, R6, R7);
  else if (node->GetOperator() == MINUS_OP)
    SUB(R5, R6, R7);
  else
    FATAL_ERROR();

  POP(R6);
  POP(R7);
}

/* This might be a case where VAR might not work */
void CodeGenerationVisitor::Visit(Assignment *node) {
  std::string var_name = node->GetID()->GetName();
  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  STRL(R5, VAR(var_name));
}

/* insert a BranchAlways to break_label_ */
void CodeGenerationVisitor::Visit(BreakStatement *node) {
  B(break_label_);
  UNUSED(node);
  /* assumption testing */
  assert(strcmp(break_label_.c_str(), ""));
}

/* R5 should be true/false after this method */
void CodeGenerationVisitor::Visit(ConditionalAndExpression *node) {
  std::string past_expr = ArmFactory::GetLabel();

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  CMP(R5, CONST(0));
  BEQ(past_expr);

  child = node->GetSecond();
  child->AcceptVisit(this);
  /* At this point the first conject must be true, therefore
   * the second conjunct value (0/1) is the and value */
  OUTPUT_LABEL(past_expr);
}

/* R5 should be true/false after this method */
void CodeGenerationVisitor::Visit(ConditionalOrExpression *node) {
  std::string past_expr = ArmFactory::GetLabel();

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  CMP(R5, CONST(1));
  BEQ(past_expr);

  child = node->GetSecond();
  child->AcceptVisit(this);
  /* if we are here, then the first disjunct was false
   * the value of the second disjunct determines the disjuction */
  OUTPUT_LABEL(past_expr);
}

/* R5 should be equal to true or false after this method */
void CodeGenerationVisitor::Visit(EqualityExpression *node) {
  PUSH(R7);
  PUSH(R6);

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  PUSH(R5);

  child = node->GetSecond();
  child->AcceptVisit(this);

  MOV(R7, R5);
  POP(R6);
  /* R5 = the boolean result for "==" or "!=" */
  if (node->GetOperator() == EQ_OP)
    EQ(R5, R6, R7);
  else if (node->GetOperator() == NE_OP)
    NE(R5, R6, R7);

  POP(R6);
  POP(R7);
}

/* note to self: if you looking for a bug, look here */
void CodeGenerationVisitor::Visit(FunctionInvocation *node) {
  std::string func_name = node->GetID()->GetName();
  std::string func_label = function_labels_[func_name];

  COMMENT("FUNC INVOKE " + func_name + " START");
  /* Push registers used in call
   * If this is not done the code foo(bar(), baz()) will
   * clobber foos registers. ie we move bar()'s result
   * into R0, then call baz(). overwriting R0 */
  PUSH(R4);
  PUSH(R3);
  PUSH(R2);
  PUSH(R1);
  PUSH(R0);

  /* Hurray for only supporting 5 parameter passing
   * I really should just implement CDECL
   * So this is what switch fallthrough is used for...
   */
  ArgumentList *args_list = node->GetArgumentList();
  Node *child;
  if (args_list->Length() >= 1) {
    child = args_list->GetArgument(0);
    child->AcceptVisit(this);
    MOV(R0, R5);
  }
  if (args_list->Length() >= 2) {
    child = args_list->GetArgument(1);
    child->AcceptVisit(this);
    MOV(R1, R5);
  }
  if (args_list->Length() >= 3) {
    child = args_list->GetArgument(2);
    child->AcceptVisit(this);
    MOV(R2, R5);
  }
  if (args_list->Length() >= 4) {
    child = args_list->GetArgument(3);
    child->AcceptVisit(this);
    MOV(R3, R5);
  }
  if (args_list->Length() >= 5) {
    child = args_list->GetArgument(4);
    child->AcceptVisit(this);
    MOV(R4, R5);
  }
  if (args_list->Length() > 5)
    FATAL_ERROR();

  BL(func_label);
  MOV(R5, R0);

  POP(R0);
  POP(R1);
  POP(R2);
  POP(R3);
  POP(R4);
  COMMENT("FUNC INVOKE " + func_name + " END");
}

/* format for if-else:
 * 		@calculate expression into R5
 *       CMP   R5, FALSE
 *       BEQ   else_statment
 *       @IF BODY
 *       B     end_ifelse
 * else_body:
 *       @ELSE BODY
 * end_ifelse:
 *       @THE END
 */
void CodeGenerationVisitor::Visit(IfElseStatement *node) {
  std::string else_label = ArmFactory::GetLabel();
  std::string end_label = ArmFactory::GetLabel();
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  /* true/false should be in R5 */
  CMP(R5, CONST(0));
  BEQ(else_label);
  child = node->GetIfStatement();
  child->AcceptVisit(this);
  B(end_label);
  OUTPUT_LABEL(else_label);
  child = node->GetElseStatement();
  child->AcceptVisit(this);
  OUTPUT_LABEL(end_label);
}

/* format for if:
 *		@calculate expression with result it R5
 *       CMP   R5, FALSE
 *       BE    past_if_body
 *       @IF BODY
 * past_if_body:
 *       @the end
 */
void CodeGenerationVisitor::Visit(IfStatement *node) {
  std::string end_label = ArmFactory::GetLabel();
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  /* true/false now in R5 */
  CMP(R5, CONST(0))
  BEQ(end_label);
  /* if body */
  child = node->GetStatement();
  child->AcceptVisit(this);
  /* if end */
  OUTPUT_LABEL(end_label);
}

/* this function is responsible for
 * A) adding strings to the local string list
 *    so that the strings can be added to local loop
 * B) loading the appropriate value into R5
 * 	  for strings this value is the label
 *    for booleans it is the value 1 or 0
 * 	  and for integers it is just the value */
void CodeGenerationVisitor::Visit(Literal *node) {
  unsigned int type = node->GetLitType();
  std::string val = node->GetValue();

  if (type == STRING_L) {
    /* If the string doesn't already exist */
    if (local_strings_.count(val) == 0)
      local_strings_[val] = ArmFactory::GetStringLabel();
    ADRL(R5, local_strings_[val]);
  } else if (type == NUMBER_L) {
    LDR(R5, LIT(val));
  } else if (type == TRUE_L) {
    LDR(R5, LIT(TRUE_VAL));
  } else if (type == FALSE_L) {
    LDR(R5, LIT(FALSE_VAL));
  } else {
    FATAL_ERROR();
  }
}

void CodeGenerationVisitor::Visit(MultiplicativeExpression *node) {
  PUSH(R7);
  PUSH(R6);

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  PUSH(R5);

  child = node->GetSecond();
  child->AcceptVisit(this);
  MOV(R7, R5);
  POP(R6);

  if (node->GetOperator() == MUL_OP)
    SMUL(R5, R6, R7);
  else if (node->GetOperator() == DIV_OP)
    SDIV(R5, R6, R7);
  else if (node->GetOperator() == PERCENT_OP)
    MOD(R5, R6, R7);
  else
    FATAL_ERROR();

  POP(R6);
  POP(R7);
}

/* I'm fairly sure that NULLNODE is only used
 * in the case of a return statement with no return
 * value. It was planned to represent a sentinal value
 * in the tree, but that was the only case I needed it */
void CodeGenerationVisitor::Visit(NullStmt *node) {
  /* Put false/0 in R5 for void returns */
  LDR(R5, LIT(FALSE_VAL));
  UNUSED(node);
}

/* R5 = true/false after this */
void CodeGenerationVisitor::Visit(RelationalExpression *node) {
  PUSH(R6);

  std::string true_label = ArmFactory::GetLabel();
  std::string end_label = ArmFactory::GetLabel();
  unsigned int op = node->GetOperator();

  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  PUSH(R5);

  child = node->GetSecond();
  child->AcceptVisit(this);
  MOV(R6, R5);
  POP(R5);
  CMP(R5, R6);

  if (op == GT_OP)
    BGT(true_label);
  else if (op == GE_OP)
    BGE(true_label);
  else if (op == LT_OP)
    BLT(true_label);
  else if (op == LE_OP)
    BLE(true_label);
  else
    FATAL_ERROR(); /* assumption testing */

  /* Output false case */
  LDR(R5, LIT(FALSE_VAL));
  B(end_label);
  OUTPUT_LABEL(true_label);
  /* Output true case */
  LDR(R5, LIT(TRUE_VAL));
  OUTPUT_LABEL(end_label);

  POP(R6);
}

/* Visit the statement attached to the return, if the return type
 * is void, then you will visit a NULLNODE and simply put 0 in R5
 * otherwise you should either load a boolean/integer expression */
void CodeGenerationVisitor::Visit(ReturnStatement *node) {
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  MOV(R0, R5);
  B(func_end_label_);
}


void CodeGenerationVisitor::Visit(UnaryExpression *node) {
  unsigned int op = node->GetOperator();
  Node *child = node->GetFirst();
  child->AcceptVisit(this);

  /* oddly enough the number is always negated
   * so it is hoisted from the if/elseif */
  NEG(R5, R5);
  if (op == EXCLAM_OP) {
    /* how boolean negation works:
     * 	the bool has been negated, so TRUE is now -1 and FALSE is still 0
     * FALSE = 0 = NEG(0) = 0 + 1 = 1
     * TRUE = 1 = NEG(1) = -1 + 1 = 0
     */
    ADD(R5, R5, CONST(1));
  } else if (op == MINUS_OP) {
    /* the number is already negated !*/
  } else {
    FATAL_ERROR();
  }
}

/* Node visitors below this point are generally unimportant */

void CodeGenerationVisitor::Visit(VariableDecl *node) {
  /* init the varible to zero if it is local
   * if it is global leave it, touching global would be
   * bad times */
  std::string id = node->GetName();
  if (local_var_offset_.count(id) == 1) {
    LDR(R5, LIT(std::string("0")));
    STRL(R5, VAR(id));
  }
}

void CodeGenerationVisitor::Visit(Block *node) {
  Node *child;
  child = node->GetBlockStatements();
  child->AcceptVisit(this);
}

void CodeGenerationVisitor::Visit(BlockStatement *node) {
  Node *child = node->GetNode();
  child->AcceptVisit(this);
}

void CodeGenerationVisitor::Visit(BlockStatements *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetStatement(i);
    child->AcceptVisit(this);
  }
}

void CodeGenerationVisitor::Visit(Ident *node) {
  LDR(R5, VAR(node->GetName()));
}

/* Generally the nodes below are used in
 * multiple contexts and do not require a visitor.
 * if information from these nodes is needed they
 * are accessed directly as opposed to visiting them */

/* the argument list will be explicted used in
 * FunctionInvocation's visitor but the visitor is not. */
void CodeGenerationVisitor::Visit(ArgumentList *node) {
  /* nothing todo */
  UNUSED(node);
  FATAL_ERROR();
}

void CodeGenerationVisitor::Visit(FormalParam *node) {
  /* nothing todo */
  UNUSED(node);
  FATAL_ERROR();
}

void CodeGenerationVisitor::Visit(FormalParamList *node) {
  /* nothing todo */
  UNUSED(node);
  FATAL_ERROR();
}

/* function declaration  is used as the entry point
 * for functions */
void CodeGenerationVisitor::Visit(FunctionDeclarator *node) {
  /* nothing todo*/
  UNUSED(node);
  FATAL_ERROR();
}

/* function declaration  is used as the entry point
 * for functions */
void CodeGenerationVisitor::Visit(FunctionHeader *node) {
  /* nothing todo */
  UNUSED(node);
  FATAL_ERROR();
}

void CodeGenerationVisitor::Visit(Type *node) {
  /* nothing todo */
  UNUSED(node);
  FATAL_ERROR();
}

/* main function decl is visited for the main function */
void CodeGenerationVisitor::Visit(MainFunctionDeclarator *node) {
  /* nothing todo*/
  UNUSED(node);
  FATAL_ERROR();
}

void CodeGenerationVisitor::Visit(DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
