/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/codeelimvisitor.h"
#include "AST/AST.h"
#include "utils/glog.h"

using namespace AST;

CodeElimVisitor::CodeElimVisitor() {
  func_aborted_ = false;
  while_aborted_ = false;
}

CodeElimVisitor::~CodeElimVisitor() { /* nothing todo */ }

void CodeElimVisitor::Visit(AST::Node *node) {
  /* lets make a recursive call to suppress function unused warnings */
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::AdditiveExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::ArgumentList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::Assignment *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::Block *node) {
  Node *child = node->GetBlockStatements();
  child->AcceptVisit(this);
}

void CodeElimVisitor::Visit(AST::BlockStatement *node) {
  Node *child = node->GetNode();
  child->AcceptVisit(this);
}

void CodeElimVisitor::Visit(AST::BlockStatements *node) {
  int i, j, len;
  Node *child;
  len = node->Length();
  for (i = 0; i < len; ++i) {
    child = node->GetStatement(i);
    child->AcceptVisit(this);
    if (func_aborted_ || while_aborted_)
      break;
  }
  /* if we break before i == len then we delete the rest */
  i = i + 1;
  if (i < len) {
    GWarning("%d: ~%d unreachable statments near line %d",
             node->GetStatement(i)->GetLineNumber(), len - i,
             node->GetStatement(i)->GetLineNumber());
  }
  /* the list is always shifting back, so just delete the same index
   * repeatedly */
  for (j = i; j < len; ++j) {
    child = node->Erase(i);
    delete child;
  }
}

void CodeElimVisitor::Visit(AST::BreakStatement *node) {
  UNUSED(node);
  while_aborted_ = true;
}

void CodeElimVisitor::Visit(AST::ConditionalAndExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::ConditionalOrExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::EqualityExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::FormalParam *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::FormalParamList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::FunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::FunctionDecl *node) {
  func_aborted_ = false;
  Node *child = node->GetBlock();
  child->AcceptVisit(this);
  func_aborted_ = false;
}

void CodeElimVisitor::Visit(AST::FunctionHeader *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::FunctionInvocation *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::Ident *node) {
  UNUSED(node);
  /* nothing todo */
}

/* only in the case where both the if/else return (or break)
 * then everything else is junk */
void CodeElimVisitor::Visit(AST::IfElseStatement *node) {
  assert(!func_aborted_ && !while_aborted_);
  bool old_func, old_while;

  Node *child = node->GetIfStatement();
  child->AcceptVisit(this);

  old_func = func_aborted_;
  old_while = while_aborted_;
  func_aborted_ = false;
  while_aborted_ = false;

  child = node->GetElseStatement();
  child->AcceptVisit(this);

  func_aborted_ = (func_aborted_ && old_func);
  while_aborted_ = (while_aborted_ && old_while);
}

/* if is not gaurenteed to run, so clear the
 * unreachable flags after visiting */
void CodeElimVisitor::Visit(AST::IfStatement *node) {
  Node *child = node->GetStatement();
  child->AcceptVisit(this);
  func_aborted_ = false;
  while_aborted_ = false;
}

void CodeElimVisitor::Visit(AST::Literal *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::MainFunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::MainFunctionDecl *node) {
  func_aborted_ = false;

  Node *child = node->GetBlock();
  child->AcceptVisit(this);

  func_aborted_ = false;
}

void CodeElimVisitor::Visit(AST::MultiplicativeExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::NullStmt *node) {
  UNUSED(node);
  FATAL_ERROR(); // don't think we should be here.
}

void CodeElimVisitor::Visit(AST::RelationalExpression *node) {
  UNUSED(node);
  /* nothing todo */
}

void CodeElimVisitor::Visit(AST::ReturnStatement *node) {
  UNUSED(node);
  func_aborted_ = true;
}

void CodeElimVisitor::Visit(AST::RootNode *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetNode(i);
    child->AcceptVisit(this);
    assert(!func_aborted_ && !while_aborted_);
  }
}
void CodeElimVisitor::Visit(AST::Type *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void CodeElimVisitor::Visit(AST::UnaryExpression *node) {
  UNUSED(node);
  /* nothing todo */
}
void CodeElimVisitor::Visit(AST::VariableDecl *node) {
  UNUSED(node);
  /* nothing todo */
}

/* While loop is not garenteed to run, so set func_aborted_ to false
 * afterward */
void CodeElimVisitor::Visit(AST::WhileStatement *node) {
  assert(!while_aborted_ && !func_aborted_);
  Node *child = node->GetStatement();
  child->AcceptVisit(this);

  while_aborted_ = false;
  func_aborted_ = false;
}

void CodeElimVisitor::Visit(AST::DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
