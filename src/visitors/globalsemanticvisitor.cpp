/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/globalsemanticvisitor.h"
#include "AST/AST.h"
#include "compiler/compiler.h"
#include "utils/glog.h"
#include <string>

using namespace AST;

/* Visit all global nodes, building the global symbol table, and
 * verifying that there is exactly one main function
 */
void GlobalSemanticVisitor::Visit(RootNode *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetNode(i);
    assert(child);
    child->AcceptVisit(this);
  }

  /* after walking the global nodes, verify we have found main */
  if (!Ctx.Symbols->HasMain())
    Ctx.Diag(Error, "no main declaration found", InvalidLoc);
}

/* put the variable decl it the symbol table
 * verify it does not already exist
 */
void GlobalSemanticVisitor::Visit(VariableDecl *node) {
  std::string var_id = node->GetName();
  /* check if the variable is getting redefined */
  if (Ctx.Symbols->HasGlobalVar(var_id)) {
    VariableDecl *other = Ctx.Symbols->GetGlobalVar(var_id);
    Ctx.Diag(diag::err_variable_redefined, node->GetLineNumber()) << var_id;
    Ctx.Diag(diag::note_declared_here, other->GetLineNumber());
    return;
  }
  /* Otherwise, add the decl to the symbol table */
  Ctx.Symbols->SetGlobalVar(node);
}

/* Check if main is already defined, if so raise a fuss;
 * otherwise add main to the symbol table.
 */
void GlobalSemanticVisitor::Visit(MainFunctionDeclarator *node) {
  std::string main_id = node->GetName();

  /* check if main is being redefined */
  if (Ctx.Symbols->HasMain()) {
    MainFunctionDeclarator *other_main = Ctx.Symbols->GetMain();
    std::string other_id = other_main->GetName();
    Ctx.Diag(Error, "main redefined as %0", node->GetLineNumber()) << main_id;
    Ctx.Diag(diag::note_declared_here, other_main->GetLineNumber());
    return;
  }

  /* check if the main function causes a name collision */
  if (Ctx.Symbols->HasFunction(main_id)) {
    FunctionHeader *func = Ctx.Symbols->GetFunction(main_id);
    std::string func_id = func->GetName();
    Ctx.Diag(Error, "main redefined as %0", node->GetLineNumber()) << main_id;
    Ctx.Diag(diag::note_declared_here, func->GetLineNumber());
    return;
  }

  /* Otherwise, if no error, add main to the sym table
   * and create a local sym table for it
   */
  Ctx.Symbols->SetMain(node);
  Ctx.Symbols->CreateLocalTable(main_id);
  Ctx.Symbols->UnloadLocalTable();
}

/* main function checking done in Visit(MainFunctionDeclarator) */
void GlobalSemanticVisitor::Visit(MainFunctionDecl *node) {
  MainFunctionDeclarator *decl = node->GetDeclarator();
  decl->AcceptVisit(this);
}

/* checking is done in Visit(FunctionHeader) */
void GlobalSemanticVisitor::Visit(FunctionDecl *node) {
  FunctionHeader *header = node->GetHeader();
  header->AcceptVisit(this);
}

/* Check if function is being redefined or conflicts with main
 * If it doesn't start by creating a local symbol table that includes the params
 */
void GlobalSemanticVisitor::Visit(FunctionHeader *node) {
  std::string func_id = node->GetName();
  /* check if it is already defined as main */
  if (Ctx.Symbols->HasMain()) {
    MainFunctionDeclarator *main_func = Ctx.Symbols->GetMain();
    std::string main_id = main_func->GetName();
    /* If main_id matches func_id then we are redefining main */
    if (main_id == func_id) {
      Ctx.Diag(Error, "function '%0' redefined as main", node->GetLineNumber())
          << func_id;
      Ctx.Diag(Note, "previously declared as main function here",
               main_func->GetLineNumber());
      return;
    }
  }

  /* Check if it is already defined as a regular function */
  if (Ctx.Symbols->HasFunction(func_id)) {
    FunctionHeader *other_func = Ctx.Symbols->GetFunction(func_id);
    Ctx.Diag(Error, "function %0 already defined", node->GetLineNumber())
        << func_id;
    Ctx.Diag(diag::note_declared_here, other_func->GetLineNumber());
    return;
  }

  /* if no error, add this as a function to the sym table */
  Ctx.Symbols->SetFunction(node);
  /* and create local table with params by visiting the declarator */
  FunctionDeclarator *fdecl = node->GetDeclarator();
  fdecl->AcceptVisit(this);
}

/* Create a local symbol table for the function, but just
 * add the parameters
 */
void GlobalSemanticVisitor::Visit(FunctionDeclarator *node) {
  /* just create the local table, it shouldn't already exist */
  std::string func_id = node->GetID()->GetName();
  Ctx.Symbols->CreateLocalTable(func_id);
  /* add the parameters by visiting the paramlist */
  FormalParamList *param_list = node->GetParamList();
  if (param_list->Length() > 5) {
    Ctx.Diag(Error, "function %0 cannot have more than 5 parameters. has %1",
             node->GetLineNumber())
        << node->GetID()->GetName() << param_list->Length();
  }
  param_list->AcceptVisit(this);
  /* unset the local table */
  Ctx.Symbols->UnloadLocalTable();
}

/* add all the parameters to the local symbol table
 * by calling Visit(FormalParam)
 */
void GlobalSemanticVisitor::Visit(FormalParamList *node) {
  FormalParam *param;
  for (int i = 0; i < node->Length(); ++i) {
    param = node->GetParam(i);
    param->AcceptVisit(this);
  }
}

/* Add a param to the (already loaded/set) local sym_table
 * note: we have to sort of hack and create a VariableDecl type
 * This will cause a memory leak, and should be fixed if I have time
 * TODO
 */
void GlobalSemanticVisitor::Visit(FormalParam *node) {
  std::string var_id = node->GetID()->GetName();
  /* Check if the var is already defined locally */
  if (Ctx.Symbols->HasLocalVar(var_id)) {
    VariableDecl *other = Ctx.Symbols->GetLocalVar(var_id);
    Ctx.Diag(diag::err_variable_redefined, node->GetLineNumber()) << var_id;
    Ctx.Diag(diag::note_declared_here, other->GetLineNumber());
    return;
  }

  /* BAD BAD BAD, should redefine all formal params as VariableDecl while
   * building the tree. Oh well...
   */
  VariableDecl *decl =
      new VariableDecl(node->GetType(), node->GetID(), node->GetLineNumber());
  Ctx.Symbols->SetLocalVar(decl);
}

/* We don't want to visit a node (ie a visitor method for a subclass was
 * forgotten) or a NullStmt (something bad happened building the tree, I don't
 * think it can happen)
 */
void GlobalSemanticVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(NullStmt *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* These are not implemented, the will never be visited in this visitor
 * as they either cannot be at a global scope, or are checked in another
 * semantic visitor
 */
void GlobalSemanticVisitor::Visit(AdditiveExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(ArgumentList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(Assignment *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(Block *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(BlockStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(BlockStatements *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(BreakStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(ConditionalAndExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(ConditionalOrExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(EqualityExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(FunctionInvocation *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(Ident *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(IfElseStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(IfStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(Literal *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(MultiplicativeExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(RelationalExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(ReturnStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(Type *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(UnaryExpression *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(WhileStatement *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void GlobalSemanticVisitor::Visit(DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
