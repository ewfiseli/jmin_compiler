/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef CODEGENERATIONVISITOR_H
#define CODEGENERATIONVISITOR_H

/* Author: Eric Fiselier
 * This is an AST visitor that generates code.
 * ToString() returns a string representation of the code
 * generated. The .cpp file has much more documentation
 */

#include "compiler/compiler.h"
#include "visitors/nodevisitor.h"
#include <map>
#include <sstream>
#include <string>

class ArmFactory;
class SymbolTable;
using jmin::Context;

class CodeGenerationVisitor : public NodeVisitor {
public:
  CodeGenerationVisitor(Context &Ctx);

  std::string ToString();

  void Visit(AST::Node *node);
  void Visit(AST::AdditiveExpression *node);
  void Visit(AST::ArgumentList *node);
  void Visit(AST::Assignment *node);
  void Visit(AST::Block *node);
  void Visit(AST::BlockStatement *node);
  void Visit(AST::BlockStatements *node);
  void Visit(AST::BreakStatement *node);
  void Visit(AST::ConditionalAndExpression *node);
  void Visit(AST::ConditionalOrExpression *node);
  void Visit(AST::EqualityExpression *node);
  void Visit(AST::FormalParam *node);
  void Visit(AST::FormalParamList *node);
  void Visit(AST::FunctionDeclarator *node);
  void Visit(AST::FunctionDecl *node);
  void Visit(AST::FunctionHeader *node);
  void Visit(AST::FunctionInvocation *node);
  void Visit(AST::Ident *node);
  void Visit(AST::IfElseStatement *node);
  void Visit(AST::IfStatement *node);
  void Visit(AST::Literal *node);
  void Visit(AST::MainFunctionDeclarator *node);
  void Visit(AST::MainFunctionDecl *node);
  void Visit(AST::MultiplicativeExpression *node);
  void Visit(AST::NullStmt *node);
  void Visit(AST::RelationalExpression *node);
  void Visit(AST::ReturnStatement *node);
  void Visit(AST::RootNode *node);
  void Visit(AST::Type *node);
  void Visit(AST::UnaryExpression *node);
  void Visit(AST::VariableDecl *node);
  void Visit(AST::WhileStatement *node);
  void Visit(AST::DeclRefExpr *node);

private:
  std::string _GetVarString(std::string id);
  void _BuildGlobalSection();
  void _BuildLocalPoolStrings();
  void _InitGlobalTablesAndSize();
  void _InitLocalTablesAndSize(std::string func_name);

  Context &Ctx;
  /* generated  code goes here */
  std::stringstream code_;
  /* for storing labels that might need to be loaded later */
  std::string main_label_;
  std::map<std::string, std::string> function_labels_;
  std::map<std::string, std::string> local_strings_;
  /* for storing local variable offsets
   * local offsets should be from FP ie the frame pointer
   * the frame pointer is set to be equal to the
   * stack pointer after prologue
   * global offsets are from IP */
  std::map<std::string, int> local_var_offset_;
  std::map<std::string, int> global_var_offset_;
  /* for storing break/return exit points */
  std::string break_label_, func_end_label_;
  /* storing local/global memory sizes */
  int local_size_, global_size_;
};

#endif /* CODEGENERATIONVISITOR_H */
