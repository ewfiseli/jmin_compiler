/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/constfoldingvisitor.h"
#include "AST/AST.h"
#include "compiler/parser.h"
#include "utils/glog.h"
#include <cstdlib>
#include <sstream>

/* This visitor folds constants by walking the tree and rebuilding nodes
 * It currently leaks memory
 * It also doesn't concern itself with overflows/underflows, but
 * doesn't touch division by zero. We will let the program fail at runtime */

using namespace AST;

#define CLEAR_FOUND_TYPES                                                      \
  found_val_ = "";                                                             \
  is_lit_ = false;                                                             \
  found_lit_ = 0;

ConstFoldingVisitor::ConstFoldingVisitor() : is_lit_(false), found_lit_(0) {}

ConstFoldingVisitor::~ConstFoldingVisitor() { /* nothing todo */ }

Literal *ConstFoldingVisitor::_ProcessLit(unsigned int op,
                                          const std::string &first,
                                          const std::string &second) {
  std::stringstream ss;
  int first_int = atoi(first.c_str());
  int second_int = atoi(second.c_str());

  switch (op) {
  case PLUS_OP:
    ss << (first_int + second_int);
    break;
  case MINUS_OP:
    ss << (first_int - second_int);
    break;
  case MUL_OP:
    ss << (first_int * second_int);
    break;
  case DIV_OP:
    if (second_int == 0)
      return nullptr;
    ss << (first_int / second_int);
    break;
  case PERCENT_OP:
    if (second_int == 0)
      return nullptr;
    ss << (first_int % second_int);
    break;
  default:
    FATAL_ERROR();
  }

  return new Literal(ss.str(), NUMBER_L, 0);
}

Literal *ConstFoldingVisitor::_ProcessLit(unsigned int op,
                                          const std::string &first) {
  std::stringstream ss;
  int val = atoi(first.c_str());
  if (op == MINUS_OP)
    ss << (-val);
  else
    FATAL_ERROR();

  return new Literal(ss.str(), NUMBER_L, 0);
}

void ConstFoldingVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(AdditiveExpression *node) {
  std::string val;
  bool is_lit;
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  /* found_node_ != null means I need to replace the node */
  if (found_lit_) {
    assert(is_lit_);
    node->SetFirst(found_lit_);
    delete child;
  }
  is_lit = is_lit_;
  val = found_val_;

  CLEAR_FOUND_TYPES;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetSecond(found_lit_);
    delete child;
  }

  /* this statement should always return */
  if (is_lit && is_lit_) {
    GDebug("%s:%s:%d ADDITIVE", __FILE__, __func__, __LINE__);
    Literal *lit = _ProcessLit(node->GetOperator(), val, found_val_);
    if (lit == 0) {
      CLEAR_FOUND_TYPES;
    } else {
      is_lit_ = true;
      found_val_ = lit->GetValue();
      found_lit_ = lit;
    }
    return;
  }

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(ArgumentList *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    CLEAR_FOUND_TYPES;
    child = node->GetArgument(i);
    child->AcceptVisit(this);
    if (found_lit_) {
      assert(is_lit_);
      node->SetArgument(i, found_lit_);
      delete child;
    }
  }
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(FunctionInvocation *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetArgumentList();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
}

/* I think that a statement like ((i=1) + 1) might
 * be possible, but that will not get folded.
 * I think that allowing const folding will clobber the expression */
void ConstFoldingVisitor::Visit(Assignment *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetExpression(found_lit_);
    delete child;
  }
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(Block *node) {
  Node *child = node->GetBlockStatements();
  child->AcceptVisit(this);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(BlockStatement *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetNode();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetNode(found_lit_);
    delete child;
  }
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(BlockStatements *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    CLEAR_FOUND_TYPES;
    child = node->GetStatement(i);
    child->AcceptVisit(this);
  }
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(BreakStatement *node) {
  UNUSED(node);
  /* nothing todo */
}

/* not sure there is ever a case where we (directly) fold a conjunct */
void ConstFoldingVisitor::Visit(ConditionalAndExpression *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  child = node->GetSecond();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
}

void ConstFoldingVisitor::Visit(ConditionalOrExpression *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  child = node->GetSecond();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
}

void ConstFoldingVisitor::Visit(EqualityExpression *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetFirst(found_lit_);
    delete child;
  }

  CLEAR_FOUND_TYPES;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetSecond(found_lit_);
    delete child;
  }

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(FormalParam *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(FormalParamList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(FunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(FunctionDecl *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetBlock();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(FunctionHeader *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* CLEAR_FOUND_TYPES sets all values for a lit.
 * an indent is not a lit */
void ConstFoldingVisitor::Visit(Ident *node) {
  UNUSED(node);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(IfElseStatement *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  /* fairly sure this must be false */
  assert(!found_lit_ && !is_lit_);

  child = node->GetIfStatement();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  child = node->GetElseStatement();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(IfStatement *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  /* fairly sure this must be false */
  assert(!found_lit_ && !is_lit_);

  child = node->GetStatement();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(Literal *node) {
  CLEAR_FOUND_TYPES;
  if (node->GetLitType() == NUMBER_L) {
    found_val_ = node->GetValue();
    is_lit_ = true;
    /* we don't set found_lit_ because we don't want to
     * fold this node, it is already in the tree */
  }
}

void ConstFoldingVisitor::Visit(MainFunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(MainFunctionDecl *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetBlock();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(MultiplicativeExpression *node) {
  std::string val;
  bool is_lit;

  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  /* found_node_ != null means I need to replace the node */
  if (found_lit_) {
    assert(is_lit_);
    node->SetFirst(found_lit_);
    delete child;
  }
  is_lit = is_lit_;
  val = found_val_;

  CLEAR_FOUND_TYPES;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetSecond(found_lit_);
    delete child;
  }

  /* this statement should always return */
  if (is_lit && is_lit_) {
    Literal *lit = _ProcessLit(node->GetOperator(), val, found_val_);
    if (lit == 0) {
      CLEAR_FOUND_TYPES;
    } else {
      is_lit_ = true;
      found_val_ = lit->GetValue();
      found_lit_ = lit;
    }
    return;
  }

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(NullStmt *node) {
  UNUSED(node);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(RelationalExpression *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetFirst(found_lit_);
    delete child;
  }

  CLEAR_FOUND_TYPES;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetSecond(found_lit_);
    delete child;
  }

  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(ReturnStatement *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    node->SetExpression(found_lit_);
    delete child;
  }
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(RootNode *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    CLEAR_FOUND_TYPES;
    child = node->GetNode(i);
    child->AcceptVisit(this);
    assert(!found_lit_ && !is_lit_);
  }
}


void ConstFoldingVisitor::Visit(Type *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void ConstFoldingVisitor::Visit(UnaryExpression *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_lit_) {
    assert(is_lit_);
    found_val_ = found_lit_->GetValue();
    Literal *lit = _ProcessLit(node->GetOperator(), found_val_);
    assert(lit);
    delete found_lit_;
    found_lit_ = lit;
    return;
  }
  if (is_lit_) {
    assert(!found_lit_);
    Literal *lit = _ProcessLit(node->GetOperator(), found_val_);
    assert(lit);
    found_val_ = lit->GetValue();
    found_lit_ = lit;
    return;
  }
  /* if we are moving a literal up */
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(VariableDecl *node) {
  UNUSED(node);
  CLEAR_FOUND_TYPES;
}

void ConstFoldingVisitor::Visit(WhileStatement *node) {
  CLEAR_FOUND_TYPES;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);

  child = node->GetStatement();
  child->AcceptVisit(this);
  assert(!found_lit_ && !is_lit_);
}

void ConstFoldingVisitor::Visit(DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
