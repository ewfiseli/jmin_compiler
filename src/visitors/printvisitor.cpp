/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/printvisitor.h"
#include "AST/AST.h"
#include "utils/common.h"
#include "utils/glog.h"
#include "utils/log.h"

#define INDENT IndentStr(str_buff_)
#define BUFF_SIZE 20

#define PrintNode(...) std::cout << FormatArgs(__VA_ARGS__) << std::endl

using namespace AST;

char *PrintVisitor::IndentStr(char *str) {
  int i;
  for (i = 0; i < 2 * indent_; ++i)
    str[i] = ' ';
  str[i] = '\0';
  return str;
}

PrintVisitor::PrintVisitor() {
  str_buff_[0] = 0;
  indent_ = 0;
  error_ = 0;
}

PrintVisitor::~PrintVisitor() { /* nothing todo */ }

void PrintVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void PrintVisitor::Visit(AdditiveExpression *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-AdditiveExpression: OP=%s", INDENT,
            SymbolToString(node->GetOperator(), buff));
  PrintNode("%s|Left Operand:", INDENT);
  indent_++;
  node->GetFirst()->AcceptVisit(this);
  indent_--;
  PrintNode("%s|Right Operand:", INDENT);
  indent_++;
  node->GetSecond()->AcceptVisit(this);
  indent_--;
}

void PrintVisitor::Visit(ArgumentList *node) {
  PrintNode("%s-ArgumentList: size %d", INDENT, node->Length());
  indent_++;
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetArgument(i);
    child->AcceptVisit(this);
  }
  indent_--;
}

void PrintVisitor::Visit(Assignment *node) {
  PrintNode("%s-Assignment: ", INDENT);
  indent_++;
  node->GetID()->AcceptVisit(this);
  node->GetExpression()->AcceptVisit(this);
  indent_--;
}

void PrintVisitor::Visit(Block *node) {
  PrintNode("%s-Block: ", INDENT);
  ++indent_;
  node->GetBlockStatements()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(BlockStatement *node) {
  PrintNode("%s-BlockStatement: ", INDENT);
  ++indent_;
  node->GetNode()->AcceptVisit(this);
  --indent_;
}
void PrintVisitor::Visit(BlockStatements *node) {
  PrintNode("%s-BlockStatements: size %d", INDENT, node->Length());
  ++indent_;
  for (int i = 0; i < node->Length(); ++i)
    node->GetStatement(i)->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(BreakStatement *node) {
  UNUSED(node);
  PrintNode("%s-BreakStatement: ", INDENT);
}

void PrintVisitor::Visit(ConditionalAndExpression *node) {
  PrintNode("%s-ConditionalAndStatment: &&", INDENT);
  PrintNode("%s|Left Side:", INDENT);
  ++indent_;
  node->GetFirst()->AcceptVisit(this);
  --indent_;
  PrintNode("%s|Right Side:", INDENT);
  ++indent_;
  node->GetSecond()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(ConditionalOrExpression *node) {
  PrintNode("%s-ConditionalOrStatment: ||", INDENT);
  PrintNode("%s|Left Side:", INDENT);
  ++indent_;
  node->GetFirst()->AcceptVisit(this);
  --indent_;
  PrintNode("%s|Right Side:", INDENT);
  ++indent_;
  node->GetSecond()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(EqualityExpression *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-EqualityExpression: OP=%s", INDENT,
            SymbolToString(node->GetOperator(), buff));
  PrintNode("%s|Left Operand:", INDENT);
  indent_++;
  node->GetFirst()->AcceptVisit(this);
  indent_--;
  PrintNode("%s|Right Operand:", INDENT);
  indent_++;
  node->GetSecond()->AcceptVisit(this);
  indent_--;
}

void PrintVisitor::Visit(FormalParam *node) {
  PrintNode("%s-FormalParam: ", INDENT);
  ++indent_;
  node->GetID()->AcceptVisit(this);
  node->GetType()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(FormalParamList *node) {
  PrintNode("%s-FormalParamList: size %d", INDENT, node->Length());
  ++indent_;
  for (int i = 0; i < node->Length(); ++i)
    node->GetParam(i)->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(FunctionDeclarator *node) {
  PrintNode("%s-FormalDeclarator: ", INDENT);
  ++indent_;
  node->GetParamList()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(FunctionDecl *node) {
  PrintNode("%s-FunctionDecl:", INDENT);
  ++indent_;
  node->GetHeader()->AcceptVisit(this);
  assert(node->GetBlock());
  node->GetBlock()->AcceptVisit(this);
  --indent_;
}
void PrintVisitor::Visit(FunctionHeader *node) {
  PrintNode("%s-FunctionHeader:", INDENT);
  ++indent_;
  node->GetType()->AcceptVisit(this);
  node->GetDeclarator()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(FunctionInvocation *node) {
  PrintNode("%s-FunctionInvocation:", INDENT);
  ++indent_;
  node->GetID()->AcceptVisit(this);
  node->GetArgumentList()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(Ident *node) {
  PrintNode("%s-Ident: Name=%s", INDENT, node->GetName().c_str());
}

void PrintVisitor::Visit(IfElseStatement *node) {
  PrintNode("%s-IfElseStatement:", INDENT);
  PrintNode("%s|Expression:", INDENT);
  ++indent_;
  node->GetExpression()->AcceptVisit(this);
  --indent_;
  PrintNode("%s| IF Body:", INDENT);
  ++indent_;
  node->GetIfStatement()->AcceptVisit(this);
  --indent_;
  PrintNode("%s| ELSE Body:", INDENT);
  ++indent_;
  node->GetElseStatement()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(IfStatement *node) {
  PrintNode("%s-IfStatement:", INDENT);
  PrintNode("%s|Expression:", INDENT);
  ++indent_;
  node->GetExpression()->AcceptVisit(this);
  --indent_;
  PrintNode("%s| IF Body:", INDENT);
  ++indent_;
  node->GetStatement()->AcceptVisit(this);
  --indent_;
}
void PrintVisitor::Visit(Literal *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-Literal: Type=%s, Value=%s", INDENT,
            SymbolToString(node->GetLitType(), buff), node->GetValue().c_str());
}

void PrintVisitor::Visit(MainFunctionDeclarator *node) {
  PrintNode("%s-MainFunctionDeclarator:", INDENT);
  ++indent_;
  node->GetID()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(MainFunctionDecl *node) {
  PrintNode("%s-MainFunctionDecl:", INDENT);
  ++indent_;
  node->GetDeclarator()->AcceptVisit(this);
  node->GetBlock()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(MultiplicativeExpression *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-MultiplicativeExpression: OP=%s", INDENT,
            SymbolToString(node->GetOperator(), buff));
  PrintNode("%s|Left Operand:", INDENT);
  indent_++;
  node->GetFirst()->AcceptVisit(this);
  indent_--;
  PrintNode("%s|Right Operand:", INDENT);
  indent_++;
  node->GetSecond()->AcceptVisit(this);
  indent_--;
}

void PrintVisitor::Visit(NullStmt *node) {
  UNUSED(node);
  PrintNode("%s-NullStmt:", INDENT);
}


void PrintVisitor::Visit(RelationalExpression *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-RelationalExpression: OP=%s", INDENT,
            SymbolToString(node->GetOperator(), buff));
  PrintNode("%s|Left Operand:", INDENT);
  indent_++;
  node->GetFirst()->AcceptVisit(this);
  indent_--;
  PrintNode("%s|Right Operand:", INDENT);
  indent_++;
  node->GetSecond()->AcceptVisit(this);
  indent_--;
}

void PrintVisitor::Visit(ReturnStatement *node) {
  PrintNode("%s-ReturnStatment: (Return Type follows)", INDENT);
  ++indent_;
  node->GetExpression()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(RootNode *node) {
  PrintNode("%s-RootNode: size=%d", INDENT, node->Length());
  ++indent_;
  for (int i = 0; i < node->Length(); ++i)
    node->GetNode(i)->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(Type *node) {
  PrintNode("%s-Type: Name=%s, UID=%u", INDENT, node->GetName().c_str(),
            node->GetUID());
}

void PrintVisitor::Visit(UnaryExpression *node) {
  char buff[BUFF_SIZE];
  PrintNode("%s-UnaryExpression: OP=%s", INDENT,
            SymbolToString(node->GetOperator(), buff));
  ++indent_;
  node->GetFirst()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(VariableDecl *node) {
  PrintNode("%s-VariableDecl:", INDENT);
  ++indent_;
  node->GetType()->AcceptVisit(this);
  node->GetID()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(WhileStatement *node) {
  PrintNode("%s-WhileStatement:", INDENT);
  PrintNode("%s|Gaurd:", INDENT);
  ++indent_;
  node->GetExpression()->AcceptVisit(this);
  --indent_;
  PrintNode("%s|Body:", INDENT);
  ++indent_;
  node->GetStatement()->AcceptVisit(this);
  --indent_;
}

void PrintVisitor::Visit(DeclRefExpr *node) {
  PrintNode("%s-DeclRefExpr:", INDENT);
  ++indent_;
  node->GetID()->AcceptVisit(this);
  Type *Ty = node->GetType();
  if (Ty) {
    Ty->AcceptVisit(this);
  }
  --indent_;
}
