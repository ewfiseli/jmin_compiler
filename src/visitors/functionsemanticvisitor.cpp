/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/functionsemanticvisitor.h"
#include "AST/AST.h"
#include "compiler/parser.h"
#include "compiler/symboltable.h"
#include "utils/glog.h"

/* uncomment the definition to force the semantic checker
 * to force ALL paths to have a return */
//#define CONFIG_MUSTRETURN

using namespace AST;

FunctionSemanticVisitor::FunctionSemanticVisitor(Context &Ctx)
    : Ctx(Ctx), null_node_(false) {
  in_func_ = is_void_ = found_return_ = false;
  func_id_ = "";
  loop_depth_ = block_depth_ = 0;
}

void FunctionSemanticVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* The rest */
void FunctionSemanticVisitor::Visit(AdditiveExpression *node) {
  Node *child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(ArgumentList *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetArgument(i);
    assert(child);
    child->AcceptVisit(this);
  }
}

void FunctionSemanticVisitor::Visit(Assignment *node) {
  std::string var_id = node->GetID()->GetName();
  /* make sure var_id is in symbol table */
  if (!Ctx.Symbols->HasVar(var_id)) {
    Ctx.Diag(Error, "variable '%0' has not been declared", node->GetLineNumber())
        << var_id;
  }
}

void FunctionSemanticVisitor::Visit(Block *node) {
  ++block_depth_;
  BlockStatements *bstate = node->GetBlockStatements();
  bstate->AcceptVisit(this);
  --block_depth_;
}

void FunctionSemanticVisitor::Visit(BlockStatement *node) {
  Node *child = node->GetNode();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(BlockStatements *node) {
  BlockStatement *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetStatement(i);
    assert(child);
    child->AcceptVisit(this);
  }
}

void FunctionSemanticVisitor::Visit(BreakStatement *node) {
  /* ensure that loop_depth_ is >= 1 */
  if (loop_depth_ < 1)
    Ctx.Diag(Error, "break statement not inside loop", node->GetLineNumber());
}

void FunctionSemanticVisitor::Visit(ConditionalAndExpression *node) {
  Node *child;
  child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(ConditionalOrExpression *node) {
  Node *child;
  child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(EqualityExpression *node) {
  Node *child;
  child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(FunctionDecl *node) {
  assert(!in_func_);

  FunctionHeader *func_header = node->GetHeader();
  func_id_ = func_header->GetName();

  /* attempt to load local function table */
  Ctx.Symbols->LoadLocalTable(func_id_);
  assert(Ctx.Symbols->LocalTableLoaded());

  /* prepare is_void_ and return_found_ */
  found_return_ = false;
  Type *type = func_header->GetType();
  is_void_ = (VOID == type->GetUID());

  /* check the depth counters */
  assert(loop_depth_ == 0 && block_depth_ == 0);

  /* set in_func and make recursive call into block */
  in_func_ = true;
  int func_loc_ = func_header->GetLineNumber();

  Block *child = node->GetBlock();
  child->AcceptVisit(this);
  in_func_ = false;
  func_loc_ = -1;

  /* Unload symbol table, reset in_func_ and check good_ */
  Ctx.Symbols->UnloadLocalTable();
  /* otherwise perform checks */
  assert(loop_depth_ == 0 && block_depth_ == 0);
  /* if non-void verify return statement */
  if (!is_void_ && !found_return_) {
    Ctx.Diag(Error, "control reaches end of non-void function",
            node->GetLineNumber());
    Ctx.Diag(diag::note_func_declared_here, func_header->GetLineNumber());
  }
}

void FunctionSemanticVisitor::Visit(FunctionInvocation *node) {
  std::string func_id = node->GetID()->GetName();
  /* check if func_id == main_id */
  if (Ctx.Symbols->HasMain()) {
    MainFunctionDeclarator *main_func = Ctx.Symbols->GetMain();
    std::string main_id = main_func->GetName();
    if (main_id == func_id) {
      Ctx.Diag(Error, "main function '%0' cannot be invoked directly",
      node->GetLineNumber())
          << func_id;
    }
  }
  /* Otherwise, attempt to fetch header from symbol table */
  if (!Ctx.Symbols->HasFunction(func_id)) {
    Ctx.Diag(Error, "function '%0' has not been declared", node->GetLineNumber())
        << func_id;
  }

  /* If we have made it this far, walk the argument list */
  ArgumentList *args = node->GetArgumentList();
  args->AcceptVisit(this);
}

/* We should only visit an ident if it is being used
 * to reference a variable, not from a decl
 */
void FunctionSemanticVisitor::Visit(Ident *node) {
  assert(in_func_ && Ctx.Symbols->LocalTableLoaded());

  if (!Ctx.Symbols->HasVar(node->GetName())) {
    Ctx.Diag(Error, "variable '%0' has not been declared", node->GetLineNumber())
        << node->GetName();
  }
}

/* if both the if, and the else have a return, then we have a full return */
void FunctionSemanticVisitor::Visit(IfElseStatement *node) {
  /* Verify if expression */
  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  /* save old found_return_, and then search for return in both if/else */
  bool old_found_return = found_return_;
  found_return_ = false;
  child = node->GetIfStatement();
  child->AcceptVisit(this);

  bool if_return = found_return_;
  /* check else */
  found_return_ = false;
  child = node->GetElseStatement();
  assert(child);
  child->AcceptVisit(this);

#ifdef CONFIG_MUSTRETURN
  /* found_return iff we found return in both the if/else or we had already
   * found return
   */
  found_return_ = (old_found_return || (if_return && found_return_));
#else
  found_return_ = (old_found_return || if_return || found_return_);
#endif
}

void FunctionSemanticVisitor::Visit(IfStatement *node) {
  bool old_found = found_return_;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  child = node->GetStatement();
  child->AcceptVisit(this);
#ifdef CONFIG_MUSTRETURN
  found_return_ = old_found;
#else
  found_return_ = old_found || found_return_;
#endif
}

void FunctionSemanticVisitor::Visit(MainFunctionDecl *node) {
  func_id_ = node->GetDeclarator()->GetName();
  in_func_ = true;
  is_void_ = true;
  found_return_ = false;
  assert(loop_depth_ == 0 && block_depth_ == 0);
  Ctx.Symbols->LoadLocalTable(func_id_);
  assert(Ctx.Symbols->LocalTableLoaded());
  Block *child = node->GetBlock();
  child->AcceptVisit(this);
  Ctx.Symbols->UnloadLocalTable();
  in_func_ = false;
}

void FunctionSemanticVisitor::Visit(MultiplicativeExpression *node) {
  Node *child;
  child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(NullStmt *node) {
  UNUSED(node);
  null_node_ = true;
}

void FunctionSemanticVisitor::Visit(RelationalExpression *node) {
  Node *child;
  child = node->GetFirst();
  child->AcceptVisit(this);

  child = node->GetSecond();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(ReturnStatement *node) {
  assert(in_func_);
  null_node_ = false;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  /* if mismatch in return types */
  if (null_node_ && !is_void_) {
    Ctx.Diag(Error, "non-void return type for void function", node->GetLineNumber());
    Ctx.Diag(diag::note_func_declared_here, func_loc_);
  }
  else if (!null_node_ && is_void_) {
    Ctx.Diag(Error, "void return type for non-void function", node->GetLineNumber());
    Ctx.Diag(diag::note_func_declared_here, func_loc_);
  }

  found_return_ = true;
}

void FunctionSemanticVisitor::Visit(RootNode *node) {
  in_func_ = false;
  func_loc_ = -1;
  loop_depth_ = 0;
  block_depth_ = 0;
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetNode(i);
    assert(child);
    child->AcceptVisit(this);
  }
}

void FunctionSemanticVisitor::Visit(UnaryExpression *node) {
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
}

void FunctionSemanticVisitor::Visit(VariableDecl *node) {
  /* Ignore global vars */
  if (!in_func_)
    return;

  std::string var_id = node->GetName();

  /* check block_depth == 1 */
  if (block_depth_ != 1) {
    Ctx.Diag(Error, "local variable '%0' must be declared in outer-most block",
             node->GetLineNumber())
        << var_id;
    return;
  }

  /* check for conficts/redefines */
  /* local vars can redefine global vars */
  assert(Ctx.Symbols->LocalTableLoaded());
  if (Ctx.Symbols->HasLocalVar(var_id)) {
    VariableDecl *other = Ctx.Symbols->GetLocalVar(var_id);
    assert(other);
    Ctx.Diag(diag::err_variable_redefined, node->GetLineNumber())
        << var_id;
    Ctx.Diag(diag::note_declared_here, other->GetLineNumber());
    return;
  }

  /* otherwise, add the variable to the local sym table */
  Ctx.Symbols->SetLocalVar(node);
}

void FunctionSemanticVisitor::Visit(WhileStatement *node) {
  bool old_found = found_return_;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  ++loop_depth_;
  child = node->GetStatement();
  child->AcceptVisit(this);
  --loop_depth_;
#ifdef CONFIG_MUSTRETURN
  found_return_ = old_found;
#else
  found_return_ = (old_found || found_return_);
#endif
}

void FunctionSemanticVisitor::Visit(Literal *node) {
  UNUSED(node);
  /* nothing todo */
}

void FunctionSemanticVisitor::Visit(MainFunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(Type *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(FormalParam *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(FormalParamList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(FunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(FunctionHeader *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void FunctionSemanticVisitor::Visit(DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
