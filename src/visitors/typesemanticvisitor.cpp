/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "visitors/typesemanticvisitor.h"
#include "AST/AST.h"
#include "compiler/parser.h"
#include "compiler/symboltable.h"
#include "utils/common.h"
#include "utils/glog.h"

#define BUFF_SIZE 128

using namespace AST;

TypeSemanticVisitor::TypeSemanticVisitor(Context &Ctx) : Ctx(Ctx) {
  func_id_ = "";
  return_type_ = 0;
  found_type_ = 0;
}

TypeSemanticVisitor::~TypeSemanticVisitor() { /* nothing todo */ }

void TypeSemanticVisitor::Visit(Node *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* Verify both sides evaluate to an integer */
void TypeSemanticVisitor::Visit(AdditiveExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];
  char buff3[BUFF_SIZE];

  found_type_ = 0;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument one of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }

  found_type_ = 0;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument two of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }

  found_type_ = INT_TYPE;
}

/* pre-condition: symbol table loaded, and assignment already in symbol table */
void TypeSemanticVisitor::Visit(Assignment *node) {

  std::string var_id = node->GetID()->GetName();
  VariableDecl *var = Ctx.Symbols->GetVar(var_id);
  assert(var);

  unsigned int wanted_type = var->GetType()->GetUID();
  found_type_ = 0;

  Node *child = node->GetExpression();
  child->AcceptVisit(this);

  if (found_type_ != wanted_type) {
    char buff1[BUFF_SIZE];
    char buff2[BUFF_SIZE];
    GError("%d: Type mismatch in assignment to var %s. expected %s got %s",
           node->GetLineNumber(), var_id.c_str(),
           SymbolToString(wanted_type, buff1),
           SymbolToString(found_type_, buff2));
  }

  /* found_type_ is equal to wanted_type  at this point */
}

void TypeSemanticVisitor::Visit(Block *node) {
  Node *child = node->GetBlockStatements();
  child->AcceptVisit(this);
}

void TypeSemanticVisitor::Visit(BlockStatement *node) {
  Node *child = node->GetNode();
  child->AcceptVisit(this);
}

void TypeSemanticVisitor::Visit(BlockStatements *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetStatement(i);
    assert(child);
    child->AcceptVisit(this);
  }
}

void TypeSemanticVisitor::Visit(ConditionalAndExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];

  Node *child = node->GetFirst();
  found_type_ = 0;
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    GError(
        "%d: Type mismatch for argument one of operator &&. expected %s got %s",
        node->GetLineNumber(), SymbolToString(BOOLEAN_TYPE, buff1),
        SymbolToString(found_type_, buff2));
  }

  child = node->GetSecond();
  found_type_ = 0;
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    GError(
        "%d: Type mismatch for argument two of operator &&. expected %s got %s",
        node->GetLineNumber(), SymbolToString(BOOLEAN_TYPE, buff1),
        SymbolToString(found_type_, buff2));
  }
  /* found_type_ == BOOLEAN_TYPE */
}

void TypeSemanticVisitor::Visit(ConditionalOrExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];

  Node *child = node->GetFirst();
  found_type_ = 0;
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    GError(
        "%d: Type mismatch for argument one of operator ||. expected %s got %s",
        node->GetLineNumber(), SymbolToString(BOOLEAN_TYPE, buff1),
        SymbolToString(found_type_, buff2));
  }

  found_type_ = 0;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    GError(
        "%d: Type mismatch for argument two of operator ||. expected %s got %s",
        node->GetLineNumber(), SymbolToString(BOOLEAN_TYPE, buff1),
        SymbolToString(found_type_, buff2));
  }
  /* found_type_ == BOOLEAN_TYPE */
}

void TypeSemanticVisitor::Visit(EqualityExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];
  unsigned int first_type;

  found_type_ = 0;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE && found_type_ != BOOLEAN_TYPE) {
    GError("%d: Illegal type \'%s\' for argument one of operator \'%s\'",
           node->GetLineNumber(), SymbolToString(found_type_, buff1),
           SymbolToString(node->GetOperator(), buff2));
  }
  first_type = found_type_;

  found_type_ = 0;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE && found_type_ != BOOLEAN_TYPE) {
    GError("%d: Illegal type \'%s\' for argument two of operator \'%s\'",
           node->GetLineNumber(), SymbolToString(found_type_, buff1),
           SymbolToString(node->GetOperator(), buff2));
  }

  /* check for matching types on left and right */
  if (found_type_ != first_type) {
    char buff3[BUFF_SIZE];
    GError("%d: Type mismatch between \'%s\' and \'%s\' for operator \'%s\'",
           node->GetLineNumber(), SymbolToString(first_type, buff1),
           SymbolToString(found_type_, buff2),
           SymbolToString(node->GetOperator(), buff3));
  }
  /* set found type. equality expressions eval to bool */
  found_type_ = BOOLEAN_TYPE;
}

/* Precondition: main is defined, and the symbol table is
 * already built
 */
void TypeSemanticVisitor::Visit(FunctionInvocation *node) {
  char buff1[128];
  char buff2[128];

  std::string func_id = node->GetID()->GetName();
  assert(Ctx.Symbols->HasMain());
  MainFunctionDeclarator *main_func = Ctx.Symbols->GetMain();
  if (main_func->GetName() == func_id)
    GError("%d: main function cannot be invoked", node->GetLineNumber());

  if (!Ctx.Symbols->HasFunction(func_id))
    GError("%d: function %s is not defined", node->GetLineNumber(),
           func_id.c_str());

  FunctionHeader *header = Ctx.Symbols->GetFunction(func_id);
  FormalParamList *params = header->GetDeclarator()->GetParamList();
  ArgumentList *args = node->GetArgumentList();
  if (args->Length() != params->Length()) {
    GError("%d: function invokation %s has too many args. got %d expected %d",
           node->GetLineNumber(), func_id.c_str(), args->Length(),
           params->Length());
  }

  unsigned int first_type;
  Node *child;
  for (int i = 0; i < args->Length(); ++i) {
    child = args->GetArgument(i);
    child->AcceptVisit(this);
    first_type = found_type_;

    found_type_ = params->GetParam(i)->GetType()->GetUID();

    if (found_type_ != first_type) {
      GError("%d: Type mismatch for argument %d of function %s. got %s "
             "expected %s",
             node->GetLineNumber(), i, func_id.c_str(),
             SymbolToString(first_type, buff1),
             SymbolToString(found_type_, buff2));
    }
  }
  /* found_type_ is function return type */
  found_type_ = header->GetType()->GetUID();
}

/* pre-condition:
 * 		we are visiting this node only from a variable reference
 * 		and the symbol table has already been properly loaded
 */
void TypeSemanticVisitor::Visit(Ident *node) {
  std::string var_id = node->GetName();
  VariableDecl *var = Ctx.Symbols->GetVar(var_id);
  assert(var);

  found_type_ = var->GetType()->GetUID();
}

void TypeSemanticVisitor::Visit(IfElseStatement *node) {
  found_type_ = 0;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    char buff1[BUFF_SIZE];
    GError("%d: if-else statement does not evaluate to a boolean. got \'%s\'",
           child->GetLineNumber(), SymbolToString(found_type_, buff1));
  }

  child = node->GetIfStatement();
  child->AcceptVisit(this);

  child = node->GetElseStatement();
  child->AcceptVisit(this);
}

void TypeSemanticVisitor::Visit(IfStatement *node) {
  found_type_ = 0;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    char buff1[BUFF_SIZE];
    GError("%d: if statement does not evaluate to a boolean. got \'%s\'",
           child->GetLineNumber(), SymbolToString(found_type_, buff1));
  }

  child = node->GetStatement();
  child->AcceptVisit(this);
}

void TypeSemanticVisitor::Visit(Literal *node) {
  unsigned int lit_type = node->GetLitType();
  if (lit_type == NUMBER_L)
    found_type_ = INT_TYPE;
  else if (lit_type == TRUE_L || lit_type == FALSE_L)
    found_type_ = BOOLEAN_TYPE;
  else if (lit_type == STRING_L)
    found_type_ = STRING_L;
  else
    FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(MainFunctionDecl *node) {
  std::string func_id_ = node->GetDeclarator()->GetName();
  Ctx.Symbols->LoadLocalTable(func_id_);
  assert(Ctx.Symbols->LocalTableLoaded());

  return_type_ = VOID;

  Node *child = node->GetBlock();
  child->AcceptVisit(this);
  Ctx.Symbols->UnloadLocalTable();
  return_type_ = 0;
}

void TypeSemanticVisitor::Visit(FunctionDecl *node) {
  std::string func_id_ = node->GetHeader()->GetName();
  Ctx.Symbols->LoadLocalTable(func_id_);
  assert(Ctx.Symbols->LocalTableLoaded());

  return_type_ = node->GetHeader()->GetType()->GetUID();

  Node *child = node->GetBlock();
  child->AcceptVisit(this);

  /* cleanup */
  Ctx.Symbols->UnloadLocalTable();
  return_type_ = 0;
}

void TypeSemanticVisitor::Visit(MultiplicativeExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];
  char buff3[BUFF_SIZE];

  found_type_ = 0;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument one of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }

  found_type_ = 0;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument two of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }
  /* multiplication expressions always result in int */
  found_type_ = INT_TYPE;
}

/* return uses a null node to indicate no return type */
void TypeSemanticVisitor::Visit(NullStmt *node) {
  UNUSED(node);
  found_type_ = VOID;
}

void TypeSemanticVisitor::Visit(RelationalExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];
  char buff3[BUFF_SIZE];

  found_type_ = 0;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument one of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }

  found_type_ = 0;
  child = node->GetSecond();
  child->AcceptVisit(this);
  if (found_type_ != INT_TYPE) {
    GError(
        "%d: Type mismatch for argument two of operator %s. expected %s got %s",
        node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
        SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }
  /* relational expressions result in bools */
  found_type_ = BOOLEAN_TYPE;
}

void TypeSemanticVisitor::Visit(ReturnStatement *node) {
  found_type_ = 0;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_type_ != return_type_) {
    char buff1[BUFF_SIZE];
    char buff2[BUFF_SIZE];
    GError("%d: type mismatch in return statement. expected \'%s\' got \'%s\'",
           node->GetLineNumber(), SymbolToString(return_type_, buff1),
           SymbolToString(found_type_, buff2));
  }
}

void TypeSemanticVisitor::Visit(RootNode *node) {
  Node *child;
  for (int i = 0; i < node->Length(); ++i) {
    child = node->GetNode(i);
    assert(child);
    child->AcceptVisit(this);
  }
}

void TypeSemanticVisitor::Visit(UnaryExpression *node) {
  char buff1[BUFF_SIZE];
  char buff2[BUFF_SIZE];
  char buff3[BUFF_SIZE];

  found_type_ = 0;
  Node *child = node->GetFirst();
  child->AcceptVisit(this);

  /* just in case, i'm paranoid, and asserts help debug */
  assert(node->GetOperator() == MINUS_OP || node->GetOperator() == EXCLAM_OP);

  if (node->GetOperator() == MINUS_OP && found_type_ != INT_TYPE) {
    GError("%d: Type mismatch for operator \'%s\'. expected \'%s\' got \'%s\'",
           node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
           SymbolToString(INT_TYPE, buff2), SymbolToString(found_type_, buff3));
  }

  if (node->GetOperator() == EXCLAM_OP && found_type_ != BOOLEAN_TYPE) {
    GError("%d: Type mismatch for operator \'%s\'. expected \'%s\' got \'%s\'",
           node->GetLineNumber(), SymbolToString(node->GetOperator(), buff1),
           SymbolToString(BOOLEAN_TYPE, buff2),
           SymbolToString(found_type_, buff3));
  }

  /* We should be good at this point */
}

void TypeSemanticVisitor::Visit(WhileStatement *node) {
  found_type_ = 0;
  Node *child = node->GetExpression();
  child->AcceptVisit(this);
  if (found_type_ != BOOLEAN_TYPE) {
    char buff1[BUFF_SIZE];
    GError("%d: while statement gaurd does not evaluate to boolean. got \'%s\'",
           node->GetLineNumber(), SymbolToString(found_type_, buff1));
  }

  child = node->GetStatement();
  child->AcceptVisit(this);
}

/* Argument list checking done in function invokation */
void TypeSemanticVisitor::Visit(ArgumentList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

/* break statement checking done in another visitor */
void TypeSemanticVisitor::Visit(BreakStatement *node) {
  UNUSED(node);
  /* nothing todo */
}

void TypeSemanticVisitor::Visit(VariableDecl *node) {
  UNUSED(node);
  /* nothing todo */
}

void TypeSemanticVisitor::Visit(FormalParam *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(FormalParamList *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(FunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(FunctionHeader *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(MainFunctionDeclarator *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(Type *node) {
  UNUSED(node);
  FATAL_ERROR();
}

void TypeSemanticVisitor::Visit(DeclRefExpr *node) {
  UNUSED(node);
  FATAL_ERROR();
}
