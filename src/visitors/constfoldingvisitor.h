/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONSTFOLDINGVISITOR_H
#define CONSTFOLDINGVISITOR_H

#include "visitors/nodevisitor.h"
#include <string>

class ConstFoldingVisitor : public NodeVisitor {
public:
  ConstFoldingVisitor();
  ~ConstFoldingVisitor();

  void Visit(AST::Node *node);
  void Visit(AST::AdditiveExpression *node);
  void Visit(AST::ArgumentList *node);
  void Visit(AST::Assignment *node);
  void Visit(AST::Block *node);
  void Visit(AST::BlockStatement *node);
  void Visit(AST::BlockStatements *node);
  void Visit(AST::BreakStatement *node);
  void Visit(AST::ConditionalAndExpression *node);
  void Visit(AST::ConditionalOrExpression *node);
  void Visit(AST::EqualityExpression *node);
  void Visit(AST::FormalParam *node);
  void Visit(AST::FormalParamList *node);
  void Visit(AST::FunctionDeclarator *node);
  void Visit(AST::FunctionDecl *node);
  void Visit(AST::FunctionHeader *node);
  void Visit(AST::FunctionInvocation *node);
  void Visit(AST::Ident *node);
  void Visit(AST::IfElseStatement *node);
  void Visit(AST::IfStatement *node);
  void Visit(AST::Literal *node);
  void Visit(AST::MainFunctionDeclarator *node);
  void Visit(AST::MainFunctionDecl *node);
  void Visit(AST::MultiplicativeExpression *node);
  void Visit(AST::NullStmt *node);
  void Visit(AST::RelationalExpression *node);
  void Visit(AST::ReturnStatement *node);
  void Visit(AST::RootNode *node);
  void Visit(AST::Type *node);
  void Visit(AST::UnaryExpression *node);
  void Visit(AST::VariableDecl *node);
  void Visit(AST::WhileStatement *node);
  void Visit(AST::DeclRefExpr *node);

private:
  AST::Literal *_ProcessLit(unsigned int op_type, const std::string &first,
                            const std::string &second);

  AST::Literal *_ProcessLit(unsigned int op_type, const std::string &first);

  std::string found_val_;
  bool is_lit_;
  AST::Literal *found_lit_;
};

#endif /* CONSTFOLDINGVISITOR_H */
