/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef TYPESEMANTICVISITOR_H
#define TYPESEMANTICVISITOR_H

#include "compiler/compiler.h"
#include "visitors/nodevisitor.h"
#include <string>

using namespace jmin;
class SymbolTable;

class TypeSemanticVisitor : public NodeVisitor {
public:
  TypeSemanticVisitor(Context &Ctx);
  ~TypeSemanticVisitor();

  void Visit(AST::Node *node);
  void Visit(AST::AdditiveExpression *node);
  void Visit(AST::ArgumentList *node);
  void Visit(AST::Assignment *node);
  void Visit(AST::Block *node);
  void Visit(AST::BlockStatement *node);
  void Visit(AST::BlockStatements *node);
  void Visit(AST::BreakStatement *node);
  void Visit(AST::ConditionalAndExpression *node);
  void Visit(AST::ConditionalOrExpression *node);
  void Visit(AST::EqualityExpression *node);
  void Visit(AST::FormalParam *node);
  void Visit(AST::FormalParamList *node);
  void Visit(AST::FunctionDeclarator *node);
  void Visit(AST::FunctionDecl *node);
  void Visit(AST::FunctionHeader *node);
  void Visit(AST::FunctionInvocation *node);
  void Visit(AST::Ident *node);
  void Visit(AST::IfElseStatement *node);
  void Visit(AST::IfStatement *node);
  void Visit(AST::Literal *node);
  void Visit(AST::MainFunctionDeclarator *node);
  void Visit(AST::MainFunctionDecl *node);
  void Visit(AST::MultiplicativeExpression *node);
  void Visit(AST::NullStmt *node);
  void Visit(AST::RelationalExpression *node);
  void Visit(AST::ReturnStatement *node);
  void Visit(AST::RootNode *node);
  void Visit(AST::Type *node);
  void Visit(AST::UnaryExpression *node);
  void Visit(AST::VariableDecl *node);
  void Visit(AST::WhileStatement *node);
  void Visit(AST::DeclRefExpr *node);

private:
  Context &Ctx;
  std::string func_id_;
  unsigned int found_type_, return_type_;
};

#endif /* TYPESEMANTICVISITOR_H */
