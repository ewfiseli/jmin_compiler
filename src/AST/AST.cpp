/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AST/AST.h"
#include "compiler/parser.h"

using namespace AST;

RootNode::~RootNode() {
  std::vector<Node *>::iterator i;

  for (i = node_list_.begin(); i != node_list_.end(); ++i) {
    if (*i)
      delete (*i);
  }
}

int RootNode::AppendNode(Node *node) {
  node_list_.push_back(node);
  return Length() - 1;
}

int RootNode::SetNode(int i, Node *node) {
  if (i == Length())
    return AppendNode(node);
  if (i < 0 || i > Length())
    return -1;

  node_list_[i] = node;
  return i;
}

Node *RootNode::GetNode(int i) {
  if (i < 0 || i >= Length())
    return nullptr;
  return node_list_[i];
}

Node *RootNode::Erase(int i) {
  Node *node;
  if (i < 0 || i > Length())
    return nullptr;
  node = node_list_[i];
  node_list_.erase(node_list_.begin() + i);
  return node;
}

Literal::Literal(std::string val, unsigned int LIT_TYPE, int line_number)
    : Expr(NodeKind::Literal, line_number), val_(val), LIT_TYPE_(LIT_TYPE) {
  assert(LIT_TYPE == STRING_L || LIT_TYPE == NUMBER_L || LIT_TYPE == TRUE_L ||
         LIT_TYPE == FALSE_L);

  val_ = val;
  LIT_TYPE_ = LIT_TYPE;
}

void Literal::SetLitType(unsigned int LIT_TYPE) {
  assert(LIT_TYPE == STRING_L || LIT_TYPE == NUMBER_L || LIT_TYPE == TRUE_L ||
         LIT_TYPE == FALSE_L);

  LIT_TYPE_ = LIT_TYPE;
}

ArgumentList::~ArgumentList() {
  std::vector<Node *>::iterator i;

  for (i = expression_list_.begin(); i != expression_list_.end(); ++i) {
    if (*i)
      delete (*i);
  }
}

int ArgumentList::AppendArgument(Node *expression) {
  expression_list_.push_back(expression);
  return Length() - 1;
}

int ArgumentList::SetArgument(int i, Node *expression) {
  if (i == Length())
    return AppendArgument(expression);
  if (i < 0 || i > Length())
    return -1;

  expression_list_[i] = expression;
  return i;
}

Node *ArgumentList::GetArgument(int i) {
  if (i < 0 || i >= Length())
    return nullptr;
  return expression_list_[i];
}

Node *ArgumentList::Erase(int i) {
  Node *exp;
  if (i < 0 || i > Length())
    return nullptr;
  exp = expression_list_[i];
  expression_list_.erase(expression_list_.begin() + i);
  return exp;
}

FormalParam::FormalParam(const FormalParam &other)
    : Node(NodeKind::FormalParam, other.GetLineNumber()) {
  assert(other.GetID() && other.GetType());
  id_ = new Ident(*other.GetID());
  type_ = new Type(*other.GetType());
}

FormalParam::~FormalParam() {
  if (id_)
    delete id_;
  if (type_)
    delete type_;
}

FormalParamList::FormalParamList(const FormalParamList &other)
    : Node(NodeKind::FormalParamList, other.GetLineNumber()) {
  FormalParam *p;
  for (int i = 0; i < other.Length(); ++i) {
    p = new FormalParam(*other.GetParam(i));
    this->AppendParam(p);
  }
}

FormalParamList::~FormalParamList() {
  std::vector<FormalParam *>::iterator i;

  for (i = fparams_.begin(); i != fparams_.end(); ++i) {
    if (*i)
      delete (*i);
  }
}

int FormalParamList::AppendParam(FormalParam *param) {
  fparams_.push_back(param);
  return Length() - 1;
}

int FormalParamList::SetParam(int i, FormalParam *param) {
  if (i == Length())
    return AppendParam(param);
  if (i < 0 || i > Length())
    return -1;

  fparams_[i] = param;
  return i;
}

FormalParam *FormalParamList::GetParam(int i) const {
  if (i < 0 || i >= Length())
    return nullptr;
  return fparams_[i];
}

FormalParam *FormalParamList::Erase(int i) {
  FormalParam *param;
  if (i < 0 || i > Length())
    return nullptr;
  param = fparams_[i];
  fparams_.erase(fparams_.begin() + i);
  return param;
}

AdditiveExpression::AdditiveExpression(Node *first, Node *second,
                                       unsigned int OPERATOR, int line_number)
    : Expr(NodeKind::AdditiveExpression, line_number) {
  assert(first);
  assert(second);
  assert(OPERATOR == MINUS_OP || OPERATOR == PLUS_OP);

  first_ = first;
  second_ = second;
  OPERATOR_ = OPERATOR;
}

AdditiveExpression::~AdditiveExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

void AdditiveExpression::SetOperator(unsigned int OPERATOR) {
  assert(OPERATOR == MINUS_OP || OPERATOR == PLUS_OP);
  OPERATOR_ = OPERATOR;
}

EqualityExpression::EqualityExpression(Node *first, Node *second,
                                       unsigned int OPERATOR, int line_number)
    : Expr(NodeKind::EqualityExpression, line_number) {
  assert(first);
  assert(second);
  assert(OPERATOR == EQ_OP || OPERATOR == NE_OP);

  first_ = first;
  second_ = second;
  OPERATOR_ = OPERATOR;
}

EqualityExpression::~EqualityExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

void EqualityExpression::SetOperator(unsigned int OPERATOR) {
  assert(OPERATOR == EQ_OP || OPERATOR == NE_OP);
  OPERATOR_ = OPERATOR;
}

MultiplicativeExpression::MultiplicativeExpression(Node *first, Node *second,
                                                   unsigned int OPERATOR,
                                                   int line_number)
    : Expr(NodeKind::MultiplicativeExpression, line_number) {
  assert(first);
  assert(second);
  assert(OPERATOR == MUL_OP || OPERATOR == DIV_OP || OPERATOR == PERCENT_OP);

  first_ = first;
  second_ = second;
  OPERATOR_ = OPERATOR;
}

MultiplicativeExpression::~MultiplicativeExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

void MultiplicativeExpression::SetOperator(unsigned int OPERATOR) {
  assert(OPERATOR == MUL_OP || OPERATOR == DIV_OP || OPERATOR == PERCENT_OP);
  OPERATOR_ = OPERATOR;
}

RelationalExpression::RelationalExpression(Node *first, Node *second,
                                           unsigned int OPERATOR,
                                           int line_number)
    : Expr(NodeKind::RelationalExpression, line_number) {
  assert(first);
  assert(second);
  assert(OPERATOR == LT_OP || OPERATOR == GT_OP || OPERATOR == LE_OP ||
         OPERATOR == GE_OP);

  first_ = first;
  second_ = second;
  OPERATOR_ = OPERATOR;
}

RelationalExpression::~RelationalExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

void RelationalExpression::SetOperator(unsigned int OPERATOR) {
  assert(OPERATOR == LT_OP || OPERATOR == GT_OP || OPERATOR == LE_OP ||
         OPERATOR == GE_OP);
  OPERATOR_ = OPERATOR;
}

Assignment::~Assignment() {
  if (id_)
    delete id_;
  if (expression_)
    delete expression_;
}

UnaryExpression::UnaryExpression(Node *first, unsigned int OPERATOR,
                                 int line_number)
    : Expr(NodeKind::UnaryExpression, line_number) {
  assert(first);
  assert(OPERATOR == MINUS_OP || OPERATOR == EXCLAM_OP);
  first_ = first;
  OPERATOR_ = OPERATOR;
}

UnaryExpression::~UnaryExpression() {
  if (first_)
    delete first_;
}

ConditionalAndExpression::~ConditionalAndExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

ConditionalOrExpression::~ConditionalOrExpression() {
  if (first_)
    delete first_;
  if (second_)
    delete second_;
}

FunctionInvocation::~FunctionInvocation() {
  if (id_)
    delete id_;
  if (args_)
    delete args_;
}

BlockStatement::~BlockStatement() {
  if (node_)
    delete node_;
}

BlockStatements::~BlockStatements() {
  std::vector<BlockStatement *>::iterator i;

  for (i = statement_list_.begin(); i != statement_list_.end(); ++i) {
    if (*i)
      delete (*i);
  }
}

int BlockStatements::AppendStatement(BlockStatement *statement) {
  statement_list_.push_back(statement);
  return Length() - 1;
}

int BlockStatements::SetStatement(int i, BlockStatement *statement) {
  if (i == Length())
    return AppendStatement(statement);
  if (i < 0 || i > Length())
    return -1;

  statement_list_[i] = statement;
  return i;
}

BlockStatement *BlockStatements::GetStatement(int i) {
  if (i < 0 || i >= Length())
    return nullptr;
  return statement_list_[i];
}

BlockStatement *BlockStatements::Erase(int i) {
  BlockStatement *statement;
  if (i < 0 || i > Length())
    return nullptr;
  statement = statement_list_[i];
  statement_list_.erase(statement_list_.begin() + i);
  return statement;
}

Block::~Block() {
  if (bstatements_)
    delete bstatements_;
}

IfStatement::~IfStatement() {
  if (expression_)
    delete expression_;
  if (statement_)
    delete statement_;
}

IfElseStatement::~IfElseStatement() {
  if (expression_)
    delete expression_;
  if (if_statement_)
    delete if_statement_;
  if (else_statement_)
    delete else_statement_;
}

ReturnStatement::~ReturnStatement() {
  if (expression_)
    delete expression_;
}

WhileStatement::~WhileStatement() {
  if (expression_)
    delete expression_;
  if (statement_)
    delete statement_;
}

VariableDecl::VariableDecl(const VariableDecl &other)
    : Decl(NodeKind::VariableDecl, other.GetLineNumber()) {
  id_ = new Ident(*other.GetID());
  type_ = new Type(*other.GetType());
}

VariableDecl::~VariableDecl() {
  if (id_)
    delete id_;
  if (type_)
    delete type_;
}

FunctionDeclarator::FunctionDeclarator(const FunctionDeclarator &other)
    : Node(other.getKind(), other.GetLineNumber()) {
  assert(other.GetID() && other.GetParamList());
  id_ = new Ident(*other.GetID());
  param_list_ = new FormalParamList(*other.GetParamList());
}

FunctionDeclarator::~FunctionDeclarator() {
  if (id_)
    delete id_;
  if (param_list_)
    delete param_list_;
}

FunctionHeader::FunctionHeader(const FunctionHeader &other)
    : Node(NodeKind::FunctionHeader, other.GetLineNumber()) {
  assert(other.GetType() && other.GetDeclarator());
  type_ = new Type(*other.GetType());
  fdecl_ = new FunctionDeclarator(*other.GetDeclarator());
}

FunctionHeader::~FunctionHeader() {
  if (type_)
    delete type_;
  if (fdecl_)
    delete fdecl_;
}

FunctionDecl::~FunctionDecl() {
  if (header_)
    delete header_;
  if (block_)
    delete block_;
}

MainFunctionDeclarator::MainFunctionDeclarator(
    const MainFunctionDeclarator &mfunc)
    : Node(NodeKind::MainFunctionDeclarator, mfunc.GetLineNumber()) {
  id_ = new Ident(*mfunc.GetID());
}

MainFunctionDeclarator::~MainFunctionDeclarator() {
  if (id_)
    delete id_;
}

MainFunctionDecl::~MainFunctionDecl() {
  if (decl_)
    delete decl_;
  if (block_)
    delete block_;
}
