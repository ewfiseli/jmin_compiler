/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef AST_AST_H
#define AST_AST_H

/*
 * Author: Eric Fiselier
 * Node Class for AST Tree
 * Has Very little functionality! It is used to implement visitor interface
 * and basic meta-data storage
 */

#include "visitors/nodevisitor.h"
#include <string>
#include <vector>
/* Undefine for no assertions in AST */
//#define NDEBUG
#include <cassert>

class NodeVisitor;

namespace AST {

enum class NodeKind {
  NODE_FIRST,
  Node,
  Null,
  Root,
  Type,
  Ident,
  Expr,
  Stmt,
  Decl,
  DeclRefExpr,
  Literal,
  ArgumentList,
  FormalParam,
  FormalParamList,
  AdditiveExpression,
  EqualityExpression,
  MultiplicativeExpression,
  RelationalExpression,
  Assignment,
  UnaryExpression,
  ConditionalAndExpression,
  ConditionalOrExpression,
  FunctionInvocation,
  BlockStatement,
  BlockStatements,
  Block,
  IfStatement,
  IfElseStatement,
  ReturnStatement,
  WhileStatement,
  BreakStatement,
  VariableDecl,
  FunctionDeclarator,
  FunctionHeader,
  FunctionDecl,
  MainFunctionDeclarator,
  MainFunctionDecl,
  NODE_LAST
};

#define DEFINE_NODE_FUNCS(Base, NK)                                            \
  static bool isKind(NodeKind K) {                                             \
    return Base::isKind(K) || K == NodeKind::NK;                               \
  }                                                                            \
  void AcceptVisit(NodeVisitor *visitor) { visitor->Visit(this); }

class Node {
public:
  Node(NodeKind Kind, int line_number)
      : Kind(Kind), line_number_(line_number) {}

  virtual ~Node() {}
  virtual void AcceptVisit(NodeVisitor *visitor) = 0;

  int GetLineNumber() const { return line_number_; }
  NodeKind getKind() const { return Kind; }

  static bool isKind(NodeKind xKind) {
    return xKind > NodeKind::NODE_FIRST && xKind < NodeKind::NODE_LAST;
  }

protected:
  NodeKind Kind;
  int line_number_;
};

template <class To> inline bool isa(const Node *N) {
  if (!N)
    return false;
  return To::isKind(N->getKind());
}

template <class To> inline const To *cast(const Node *N) {
  assert(N);
  assert(To::isKind(N->getKind()));
  return static_cast<const To *>(N);
}

template <class To> inline To *cast(Node *N) {
  assert(N);
  assert(To::isKind(N->getKind()));
  return static_cast<To *>(N);
}

template <class To> inline const To *dyn_cast(const Node *N) {
  assert(N);
  if (!To::isKind(N->getKind()))
    return nullptr;
  return static_cast<const To *>(N);
}

template <class To> inline To *dyn_cast(Node *N) {
  assert(N);
  if (!To::isKind(N->getKind()))
    return nullptr;
  return static_cast<To *>(N);
}

class Type : public Node {
public:
  Type(std::string name, unsigned int UID, int line_number)
      : Node(NodeKind::Type, line_number), UID_(UID), name_(name) {}
  Type(const Type &other) = default;

  std::string GetName() const { return name_; }
  void SetName(std::string name) { name_ = name; }
  unsigned int GetUID() const { return UID_; }
  void SetUID(unsigned int UID) { UID_ = UID; }

  DEFINE_NODE_FUNCS(Node, Type)
private:
  unsigned int UID_;
  std::string name_;
};

class Stmt : public Node {
protected:
  Stmt(NodeKind K, int Loc) : Node(K, Loc) {}

public:
  DEFINE_NODE_FUNCS(Node, Stmt)
};

class Expr : public Stmt {
protected:
  Expr(NodeKind K, int Loc) : Stmt(K, Loc) {}

public:
  DEFINE_NODE_FUNCS(Stmt, Expr)
};

class Decl : public Node {
protected:
  Decl(NodeKind K, int Loc) : Node(K, Loc) {}

public:
  DEFINE_NODE_FUNCS(Node, Decl)
};

class NullStmt : public Stmt {
public:
  NullStmt() : Stmt(NodeKind::Null, -1) {}

  DEFINE_NODE_FUNCS(Stmt, Null)
};

class RootNode : public Node {
public:
  RootNode() : Node(NodeKind::Root, -1) {}
  ~RootNode();

  std::string GetFilename() const { return filename_; }
  void SetFilename(std::string filename) { filename_ = filename; }

  /* Return < 0 if error */
  int SetNode(int i, Node *node);
  int AppendNode(Node *node);
  Node *GetNode(int i);
  Node *Erase(int i);
  int Length() const { return node_list_.size(); }

  DEFINE_NODE_FUNCS(Node, Root)
private:
  std::string filename_;
  std::vector<Node *> node_list_;
};

class Ident : public Node {
public:
  Ident(std::string name, unsigned int UID, int line_number)
      : Node(NodeKind::Ident, line_number), UID_(UID), name_(name) {}
  Ident(const Ident &other) = default;

  std::string GetName() const { return name_; }
  void SetName(std::string name) { name_ = name; }
  unsigned int GetUID() const { return UID_; }
  void SetUID(unsigned int UID) { UID_ = UID; }

  DEFINE_NODE_FUNCS(Node, Ident)
private:
  unsigned int UID_;
  std::string name_;
};

class DeclRefExpr : public Expr {
public:
  DeclRefExpr(int Loc, Ident *ID)
      : Expr(NodeKind::DeclRefExpr, Loc), id_(ID), type_(nullptr) {}

  Ident *GetID() const { return id_; }
  void SetID(Ident *ID) { id_ = ID; }

  Type *GetType() const { return type_; }
  void SetType(Type *type) { type_ = type; }

  DEFINE_NODE_FUNCS(Expr, DeclRefExpr)
private:
  Ident *id_;
  Type *type_;
};

class Literal : public Expr {
public:
  Literal(std::string val, unsigned int LIT_TYPE, int line_number);

  void SetValue(std::string val) { val_ = val; }
  std::string GetValue() const { return val_; }
  void SetLitType(unsigned int LIT_TYPE);
  unsigned int GetLitType() const { return LIT_TYPE_; }

  DEFINE_NODE_FUNCS(Expr, Literal)
private:
  std::string val_;
  unsigned int LIT_TYPE_;
};

class ArgumentList : public Node {
public:
  ArgumentList(int line_number) : Node(NodeKind::ArgumentList, line_number) {}
  ~ArgumentList();

  int AppendArgument(Node *expression);
  int SetArgument(int i, Node *expression);
  Node *GetArgument(int i);
  Node *Erase(int i);
  int Length() const { return expression_list_.size(); }

  DEFINE_NODE_FUNCS(Node, ArgumentList)
private:
  std::vector<Node *> expression_list_;
};

class FormalParam : public Node {
public:
  FormalParam(Type *type, Ident *id, int line_number)
      : Node(NodeKind::FormalParam, line_number), id_(id), type_(type) {}
  FormalParam(const FormalParam &other);
  ~FormalParam();

  void SetID(Ident *id) { id_ = id; }
  Ident *GetID() const { return id_; }
  void SetType(Type *type) { type_ = type; }
  Type *GetType() const { return type_; }

  DEFINE_NODE_FUNCS(Node, FormalParam)
private:
  Ident *id_;
  Type *type_;
};

class FormalParamList : public Node {
public:
  FormalParamList(int line_number)
      : Node(NodeKind::FormalParamList, line_number) {}
  FormalParamList(const FormalParamList &other);
  ~FormalParamList();

  int AppendParam(FormalParam *param);
  int SetParam(int i, FormalParam *param);
  FormalParam *GetParam(int i) const;
  FormalParam *Erase(int i);
  int Length() const { return fparams_.size(); }

  DEFINE_NODE_FUNCS(Node, FormalParamList)
private:
  std::vector<FormalParam *> fparams_;
};

class AdditiveExpression : public Expr {
public:
  AdditiveExpression(Node *first, Node *second, unsigned int OPERATOR,
                     int line_number);
  ~AdditiveExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  unsigned int GetOperator() const { return OPERATOR_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }
  void SetOperator(unsigned int OPERATOR);

  DEFINE_NODE_FUNCS(Expr, AdditiveExpression)
private:
  Node *first_, *second_;
  unsigned int OPERATOR_;
};

class EqualityExpression : public Expr {
public:
  EqualityExpression(Node *first, Node *second, unsigned int OPERATOR,
                     int line_number);
  ~EqualityExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  unsigned int GetOperator() const { return OPERATOR_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }
  void SetOperator(unsigned int OPERATOR);

  DEFINE_NODE_FUNCS(Expr, EqualityExpression)
private:
  Node *first_, *second_;
  unsigned int OPERATOR_;
};

class MultiplicativeExpression : public Expr {
public:
  MultiplicativeExpression(Node *first, Node *second, unsigned int OPERATOR,
                           int line_number);
  ~MultiplicativeExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  unsigned int GetOperator() const { return OPERATOR_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }
  void SetOperator(unsigned int OPERATOR);

  DEFINE_NODE_FUNCS(Expr, MultiplicativeExpression)
private:
  Node *first_, *second_;
  unsigned int OPERATOR_;
};

class RelationalExpression : public Expr {
public:
  RelationalExpression(Node *first, Node *second, unsigned int OPERATOR,
                       int line_number);
  ~RelationalExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  unsigned int GetOperator() { return OPERATOR_; }

  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }
  void SetOperator(unsigned int OPERATOR);

  DEFINE_NODE_FUNCS(Expr, RelationalExpression)
private:
  Node *first_, *second_;
  unsigned int OPERATOR_;
};

class Assignment : public Expr {
public:
  Assignment(Ident *id, Node *expression, int line_number)
      : Expr(NodeKind::Assignment, line_number), id_(id),
        expression_(expression) {}
  ~Assignment();

  void SetID(Ident *id) { id_ = id; }
  Ident *GetID() const { return id_; }

  void SetExpression(Node *expression) { expression_ = expression; }
  Node *GetExpression() const { return expression_; }

  DEFINE_NODE_FUNCS(Expr, Assignment)
private:
  Ident *id_;
  Node *expression_;
};

class UnaryExpression : public Expr {
public:
  UnaryExpression(Node *first, unsigned int OPERATOR, int line_number);
  ~UnaryExpression();

  Node *GetFirst() const { return first_; }
  unsigned int GetOperator() const { return OPERATOR_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetOperator(unsigned int OPERATOR) { OPERATOR_ = OPERATOR; }

  DEFINE_NODE_FUNCS(Expr, UnaryExpression)
private:
  Node *first_;
  unsigned int OPERATOR_;
};

class ConditionalAndExpression : public Expr {
public:
  ConditionalAndExpression(Node *first, Node *second, int line_number)
      : Expr(NodeKind::ConditionalAndExpression, line_number), first_(first),
        second_(second) {}
  ~ConditionalAndExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }

  DEFINE_NODE_FUNCS(Expr, ConditionalAndExpression)
private:
  Node *first_, *second_;
};

class ConditionalOrExpression : public Expr {
public:
  ConditionalOrExpression(Node *first, Node *second, int line_number)
      : Expr(NodeKind::ConditionalOrExpression, line_number), first_(first),
        second_(second) {}
  ~ConditionalOrExpression();

  Node *GetFirst() const { return first_; }
  Node *GetSecond() const { return second_; }
  void SetFirst(Node *first) { first_ = first; }
  void SetSecond(Node *second) { second_ = second; }

  DEFINE_NODE_FUNCS(Expr, ConditionalOrExpression)
private:
  Node *first_, *second_;
};

class FunctionInvocation : public Expr {
public:
  FunctionInvocation(Ident *id, ArgumentList *args, int line_number)
      : Expr(NodeKind::FunctionInvocation, line_number), id_(id), args_(args) {}
  FunctionInvocation();
  ~FunctionInvocation();

  void SetArgumentList(ArgumentList *args) { args_ = args; }
  ArgumentList *GetArgumentList() const { return args_; }
  void SetID(Ident *id) { id_ = id; }
  Ident *GetID() const { return id_; }

  DEFINE_NODE_FUNCS(Expr, FunctionInvocation)
private:
  Ident *id_;
  ArgumentList *args_;
};

class BlockStatement : public Stmt {
public:
  /* Can be type VariableDecl or Statement */
  BlockStatement(Node *node, int line_number)
      : Stmt(NodeKind::BlockStatement, line_number), node_(node) {}
  ~BlockStatement();

  Node *GetNode() const { return node_; }
  void SetNode(Node *node) { node_ = node; }

  DEFINE_NODE_FUNCS(Stmt, BlockStatement)
private:
  Node *node_;
};

class BlockStatements : public Stmt {
public:
  BlockStatements(int line_number)
      : Stmt(NodeKind::BlockStatements, line_number) {}
  ~BlockStatements();

  int AppendStatement(BlockStatement *block_state);
  int SetStatement(int i, BlockStatement *block_statement);
  BlockStatement *GetStatement(int i);
  BlockStatement *Erase(int i);
  int Length() const { return statement_list_.size(); }

  DEFINE_NODE_FUNCS(Stmt, BlockStatements)
private:
  std::vector<BlockStatement *> statement_list_;
};

class Block : public Stmt {
public:
  Block(BlockStatements *bstatements, int line_number)
      : Stmt(NodeKind::Block, line_number), bstatements_(bstatements) {}
  ~Block();

  BlockStatements *GetBlockStatements() const { return bstatements_; }
  void SetBlockStatements(BlockStatements *bstatements) {
    bstatements_ = bstatements;
  }

  DEFINE_NODE_FUNCS(Stmt, Block)
private:
  BlockStatements *bstatements_;
};

class IfStatement : public Stmt {
public:
  IfStatement(Node *expression, Node *statement, int line_number)
      : Stmt(NodeKind::IfStatement, line_number), expression_(expression),
        statement_(statement) {}
  ~IfStatement();

  Node *GetExpression() const { return expression_; }
  void SetExpression(Node *expression) { expression_ = expression; }
  Node *GetStatement() const { return statement_; }
  void SetStatement(Node *statement) { statement_ = statement; }

  DEFINE_NODE_FUNCS(Stmt, IfStatement)
private:
  Node *expression_;
  Node *statement_;
};

class IfElseStatement : public Stmt {
public:
  IfElseStatement(Node *expression, Node *if_statement, Node *else_statement,
                  int line_number)
      : Stmt(NodeKind::IfElseStatement, line_number), expression_(expression),
        if_statement_(if_statement), else_statement_(else_statement) {}
  ~IfElseStatement();

  Node *GetExpression() const { return expression_; }
  void SetExpression(Node *expression) { expression_ = expression; }
  Node *GetIfStatement() const { return if_statement_; }
  void SetIfStatement(Node *if_statement) { if_statement_ = if_statement; }
  Node *GetElseStatement() const { return else_statement_; }
  void SetElseStatement(Node *else_statement) {
    else_statement_ = else_statement;
  }

  DEFINE_NODE_FUNCS(Stmt, IfElseStatement)
private:
  Node *expression_;
  Node *if_statement_, *else_statement_;
};

class ReturnStatement : public Stmt {
public:
  ReturnStatement(Node *expression, int line_number)
      : Stmt(NodeKind::ReturnStatement, line_number), expression_(expression) {}
  ~ReturnStatement();

  void SetExpression(Node *expression) { expression_ = expression; }
  Node *GetExpression() const { return expression_; }

  DEFINE_NODE_FUNCS(Stmt, ReturnStatement)
private:
  Node *expression_;
};

class WhileStatement : public Stmt {
public:
  WhileStatement(Node *expression, Node *statement, int line_number)
      : Stmt(NodeKind::WhileStatement, line_number), expression_(expression),
        statement_(statement) {}

  ~WhileStatement();

  Node *GetExpression() const { return expression_; }
  Node *GetStatement() const { return statement_; }
  void SetExpression(Node *expression) { expression_ = expression; }
  void SetStatement(Node *statement) { statement_ = statement; }

  DEFINE_NODE_FUNCS(Stmt, WhileStatement)
private:
  Node *expression_, *statement_;
};

class BreakStatement : public Stmt {
public:
  BreakStatement(int line_number)
      : Stmt(NodeKind::BreakStatement, line_number) {}
  DEFINE_NODE_FUNCS(Stmt, BreakStatement)
};

class VariableDecl : public Decl {
public:
  VariableDecl(Type *type, Ident *id, int line_number)
      : Decl(NodeKind::VariableDecl, line_number), type_(type), id_(id) {}
  VariableDecl(const VariableDecl &other);
  ~VariableDecl();

  void SetID(Ident *id) { id_ = id; }
  Ident *GetID() const { return id_; }
  void SetType(Type *type) { type_ = type; }
  Type *GetType() const { return type_; }

  std::string GetName() const { return id_->GetName(); }

  DEFINE_NODE_FUNCS(Decl, VariableDecl)
private:
  Type *type_;
  Ident *id_;
};

class FunctionDeclarator : public Node {
public:
  FunctionDeclarator(Ident *id, FormalParamList *param_list, int line_number)
      : Node(NodeKind::FunctionDeclarator, line_number), id_(id),
        param_list_(param_list) {}

  FunctionDeclarator(const FunctionDeclarator &other);
  ~FunctionDeclarator();

  Ident *GetID() const { return id_; }
  void SetID(Ident *id) { id_ = id; }

  FormalParamList *GetParamList() const { return param_list_; }
  void SetParamList(FormalParamList *param_list) { param_list_ = param_list; }

  DEFINE_NODE_FUNCS(Node, FunctionDeclarator)
private:
  Ident *id_;
  FormalParamList *param_list_;
};

class FunctionHeader : public Node {
public:
  FunctionHeader(Type *type, FunctionDeclarator *fdecl, int line_number)
      : Node(NodeKind::FunctionHeader, line_number), type_(type),
        fdecl_(fdecl) {}
  FunctionHeader(const FunctionHeader &fheader);
  ~FunctionHeader();

  void SetType(Type *type) { type_ = type; }
  Type *GetType() const { return type_; }
  void SetDeclarator(FunctionDeclarator *fdecl) { fdecl_ = fdecl; }
  FunctionDeclarator *GetDeclarator() const { return fdecl_; }

  std::string GetName() const { return fdecl_->GetID()->GetName(); }

  DEFINE_NODE_FUNCS(Node, FunctionHeader)
private:
  Type *type_;
  FunctionDeclarator *fdecl_;
};

class FunctionDecl : public Decl {
public:
  FunctionDecl(FunctionHeader *header, Block *block, int line_number)
      : Decl(NodeKind::FunctionHeader, line_number), block_(block),
        header_(header) {}
  ~FunctionDecl();

  FunctionHeader *GetHeader() const { return header_; }
  void SetHeader(FunctionHeader *header) { header_ = header; }
  Block *GetBlock() const { return block_; }
  void SetBlock(Block *block) { block_ = block; }

  DEFINE_NODE_FUNCS(Decl, FunctionDecl)
private:
  Block *block_;
  FunctionHeader *header_;
};

class MainFunctionDeclarator : public Node {
public:
  MainFunctionDeclarator(Ident *id, int line_number)
      : Node(NodeKind::MainFunctionDeclarator, line_number), id_(id) {}
  MainFunctionDeclarator(const MainFunctionDeclarator &other);
  ~MainFunctionDeclarator();

  Ident *GetID() const { return id_; }
  void SetID(Ident *id) { id_ = id; }

  std::string GetName() const { return id_->GetName(); }

  DEFINE_NODE_FUNCS(Node, MainFunctionDeclarator)
private:
  Ident *id_;
};

class MainFunctionDecl : public Decl {
public:
  MainFunctionDecl(MainFunctionDeclarator *decl, Block *block, int line_number)
      : Decl(NodeKind::MainFunctionDecl, line_number), decl_(decl),
        block_(block) {}
  ~MainFunctionDecl();

  MainFunctionDeclarator *GetDeclarator() const { return decl_; }
  void SetDeclarator(MainFunctionDeclarator *decl) { decl_ = decl; }
  Block *GetBlock() const { return block_; }
  void SetBlock(Block *block) { block_ = block; }

  DEFINE_NODE_FUNCS(Decl, MainFunctionDecl)
private:
  MainFunctionDeclarator *decl_;
  Block *block_;
};

} // namespace AST

#endif // AST_NODE_H
