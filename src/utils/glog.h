/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef JMIN_GLOG_H
#define JMIN_GLOG_H
/* 
	Author: Eric Fiselier
	A class for logging messages
	Default level logs steps and greater
	Error is for compile errors and exits the program
	Fatal is for non-compile non-recoverable errors
		it also exits the program
*/

#include "utils/log.h"

class GLogFactory {
public:
	static Log& GetGlobalLog();
private:
	static Log *m_log;
	GLogFactory();
	DISALLOW_COPY_AND_ASSIGN(GLogFactory);
};
	
#define FATAL_ERROR() GRawFatal("%s:%s:%d Unknown Fatal Error", __FILE__, __func__, __LINE__)

#define GSetPrompt(level, str) GLogFactory::GetGlobalLog().SetPrompt(level, str)

#define GGetPrompt(level) GLogFactory::GetGlobalLog().GetPrompt(level)

#define GSetLevel(level) GLogFactory::GetGlobalLog().SetLevel(level)

#define GGetLevel() GLogFactory::GetGlobalLog().GetLevel()

#define GIncrementLevel(delta) GLogFactory::GetGlobalLog().IncrementLevel(delta)

#define GRawOut(...) GLogFactory::GetGlobalLog().RawOut(__VA_ARGS__)

#define GRawErr(...) GLogFactory::GetGlobalLog().RawErr(__VA_ARGS__)

#define GRawFatal(...) GLogFactory::GetGlobalLog().RawFatal(__VA_ARGS__)

#define GDebug(...) GLogFactory::GetGlobalLog().Debug(__VA_ARGS__)

#define GInfo(...) GLogFactory::GetGlobalLog().Info(__VA_ARGS__)

#define GStep(...) GLogFactory::GetGlobalLog().Step(__VA_ARGS__)

#define GWarning(...) GLogFactory::GetGlobalLog().Warning(__VA_ARGS__)

#define GError(...) GLogFactory::GetGlobalLog().Error(__VA_ARGS__)

#define GFatal(...) GLogFactory::GetGlobalLog().Fatal(__VA_ARGS__)

#endif /* JMIN_GLOG_H */