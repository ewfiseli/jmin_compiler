/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "utils/common.h"
#include "AST/AST.h"
#include "compiler/symboltable.h"
#include "compiler/parser.h"
#include <string>

using namespace AST;

/* add all the functions in the run-time library to the 
 * symbol table. prevent namespace collisions and false missing methods
 */
void AddRuntimeLibrary(SymbolTable *sym_table)
{
	assert(sym_table);
	
	/* create getchar */
	Type *type = new Type(std::string("int"), INT_TYPE, 0);
	Ident *id = new Ident(std::string("getchar"), 0, 0);
	FormalParamList *param_list = new FormalParamList(0);
	FunctionDeclarator *decl = new FunctionDeclarator(id, param_list, 0);
	FunctionHeader *header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
	
	/* create halt */
	type = new Type(std::string("void"), VOID, 0);
	id = new Ident(std::string("halt"), 0, 0);
	param_list = new FormalParamList(0);
	decl = new FunctionDeclarator(id, param_list, 0);
	header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
	
	/* create printb */
	type = new Type(std::string("void"), VOID, 0);
	id = new Ident(std::string("printb"), 0, 0);
	param_list = new FormalParamList(0);
	Type *p1_type = new Type(std::string("boolean"), BOOLEAN_TYPE, 0);
	Ident *p1_id = new Ident(std::string("b"), 0, 0);
	FormalParam *p1 = new FormalParam(p1_type, p1_id, 0);
	param_list->AppendParam(p1);
	decl = new FunctionDeclarator(id, param_list, 0);
	header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
	
	/* create printc */
	type = new Type(std::string("void"), VOID, 0);
	id = new Ident(std::string("printc"), 0, 0);
	param_list = new FormalParamList(0);
	p1_type = new Type(std::string("int"), INT_TYPE, 0);
	p1_id = new Ident(std::string("c"), 0, 0);
	p1 = new FormalParam(p1_type, p1_id, 0);
	param_list->AppendParam(p1);
	decl = new FunctionDeclarator(id, param_list, 0);
	header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
	
	/* create printi */
	type = new Type(std::string("void"), VOID, 0);
	id = new Ident(std::string("printi"), 0, 0);
	param_list = new FormalParamList(0);
	p1_type = new Type(std::string("int"), INT_TYPE, 0);
	p1_id = new Ident(std::string("i"), 0, 0);
	p1 = new FormalParam(p1_type, p1_id, 0);
	param_list->AppendParam(p1);
	decl = new FunctionDeclarator(id, param_list, 0);
	header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
	
	/* create prints */
	type = new Type(std::string("void"), VOID, 0);
	id = new Ident(std::string("prints"), 0, 0);
	param_list = new FormalParamList(0);
	p1_type = new Type(std::string("STRING_L"), STRING_L, 0);
	p1_id = new Ident(std::string("s"), 0, 0);
	p1 = new FormalParam(p1_type, p1_id, 0);
	param_list->AppendParam(p1);
	decl = new FunctionDeclarator(id, param_list, 0);
	header = new FunctionHeader(type, decl, 0);
	sym_table->SetFunction(header);
}


char *SymbolToString(unsigned int symbol, char *buff) {
	switch (symbol) {
		case VOID:
			return strcpy(buff, "void");
		case IF:
			return strcpy(buff, "if");
		case ELSE:
			return strcpy(buff, "else" );
		case WHILE:
			return strcpy(buff, "while");
		case BREAK:
			return strcpy(buff, "break");
		case RETURN:
			return strcpy(buff, "return");
		case STRING_L:
			return strcpy(buff, "STRING_L");
		case NUMBER_L:
			return strcpy(buff, "NUMBER_L");
		case TRUE_L:
			return strcpy(buff, "true");
		case FALSE_L:
			return strcpy(buff, "false");
		case BOOLEAN_TYPE:
			return strcpy(buff, "boolean");
		case INT_TYPE:
			return strcpy(buff, "int");
		case LPAREN_CC:
			return strcpy(buff, "(");
		case RPAREN_CC:
			return strcpy(buff, ")");
		case LBRACE_CC:
			return strcpy(buff, "{");
		case RBRACE_CC:
			return strcpy(buff, "}");
		case SEMICOLON_CC:
			return strcpy(buff, ";");
		case COMA_CC:
			return strcpy(buff, ",");
		case EQ_OP:
			return strcpy(buff, "==");
		case NE_OP:
			return strcpy(buff, "!=");
		case PLUS_OP:
			return strcpy(buff, "+");
		case MINUS_OP:
			return strcpy(buff, "-");
		case MUL_OP:
			return strcpy(buff, "*");
		case EXCLAM_OP:
			return strcpy(buff, "!");
		case DIV_OP:
			return strcpy(buff, "/");
		case PERCENT_OP:
			return strcpy(buff, "%");
		case LE_OP:
			return strcpy(buff, "<=");
		case GE_OP:
			return strcpy(buff, ">=");
		case LT_OP:
			return strcpy(buff, "<");
		case GT_OP:
			return strcpy(buff, ">");
		case ASSIGN_OP:
			return strcpy(buff, "=");
		case AND_OP:
			return strcpy(buff, "&&");
		case OR_OP:
			return strcpy(buff, "||");
		case ID:
			return strcpy(buff, "ID" );
		case 0:
			return strcpy(buff, "NULL");
		default:
			GWarning("SymbolToString in default case\n");
			return strcpy(buff, "BADTIMES");
	}
	
	return strcpy(buff, "");
}
