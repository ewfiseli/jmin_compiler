/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef JMIN_UTILS_LOG_H
#define JMIN_UTILS_LOG_H

#include <iostream>
#include <map>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <mutex>
#include <type_traits>

enum LevelEnum { DEBUG_L, INFO_L, STEP_L, WARNING_L, ERROR_L, FATAL_L };
enum RawLevelEnum { RAW_OUT_L=FATAL_L, RAW_ERR_L, RAW_FATAL_L };

template <class Tp, class = std::enable_if_t<std::is_same_v<std::string, std::decay_t<Tp>>>>
inline const char* UnwrapArg(Tp const& val) {
  return val.c_str();
}

template <class Tp, class = std::enable_if_t<!std::is_same_v<std::string, std::decay_t<Tp>>>>
inline Tp const& UnwrapArg(Tp const& val) {
  return val;
};

inline std::string FormatArgs(const char *fmt) {
	return std::string(fmt);
}

#define __STR_BUFF_MAX 1024
template <typename... Args>
inline std::string FormatArgs(const char *fmt, const Args&... args)
{
	char *str_buff = new char[__STR_BUFF_MAX];
	int written = -1;
	do {
		written = snprintf(str_buff, __STR_BUFF_MAX, fmt,  UnwrapArg(args)...);
		assert(written > 0);
		if (written >= __STR_BUFF_MAX) {
			delete [] str_buff;
			str_buff = new char[written+1];
		}
	} while (written >= __STR_BUFF_MAX);
	
	std::string tmp(str_buff);
	delete [] str_buff;
	return tmp;
}
#undef __STR_BUFF_MAX


class Log
{
private:
	static const int _MIN_LEVEL = DEBUG_L;
	static const int _MAX_LEVEL = RAW_FATAL_L;
public:
	static const int DEFAULT_L = STEP_L;
	
	Log() : m_level(Log::DEFAULT_L), m_out(&std::cout), m_err(&std::cerr) 
	{ }
	
	virtual ~Log() { }
	
	bool SetPrompt(int level, const std::string &prompt) {
		if (! _valid_level(level) || _is_raw_level(level))
			return false;
		m_prompts[level] = prompt;
		return true;
	}
	
	std::string GetPrompt(int level) const {
		if (! _valid_level(level) || _is_raw_level(level))
			return std::string("");
		
		return m_prompts.at(level);
	}
	
	void SetLevel(int level) { 
		m_level = level; 
	}
	
	int GetLevel() const { 
		return m_level;
	}
	
	int IncrementLevel(int delta) {
		m_level += delta;
		return m_level;
	}
	
	template<typename... Args>
	inline void Debug(const char *fmt, const Args&... args) {
		_log(*m_out, DEBUG_L, fmt, args...);
	}
	
	template<typename... Args>
	inline void Info(const char *fmt, const Args&... args) {
		_log(*m_out, INFO_L, fmt, args...);
	}
	
	template<typename... Args>
	inline void Step(const char *fmt, const Args&... args) {
		_log(*m_out, STEP_L, fmt, args...);
	}
	
	template<typename... Args>
	inline void Warning(const char *fmt, const Args&... args) {
		_log(*m_err, WARNING_L, fmt, args...);
	}
	
	template<typename... Args>
	inline void Error(const char *fmt, const Args&... args) {
		_log(*m_err, ERROR_L, fmt, args...);
		_abort();
	}
	
	template<typename... Args>
	inline void Fatal(const char *fmt, const Args&... args) {
		_log(*m_err, FATAL_L, fmt, args...);
		_abort();
	}
	
	template<typename... Args>
	inline void RawOut(const char *fmt, const Args&... args) {
		_log(*m_out, RAW_OUT_L, fmt, args...);
	}
	
	template<typename... Args>
	inline void RawErr(const char *fmt, const Args&... args) {
		_log(*m_err, RAW_ERR_L, fmt, args...);
	}
	
	template <typename... Args>
	inline void RawFatal(const char *fmt, const Args&... args) {
		_log(*m_err, RAW_FATAL_L, fmt, args...);
		_abort();
	}
	
private:
	inline void _abort() { 
		abort();
	}
	
	template <typename... Args>
	inline void _log(std::ostream &out, int level, 
					 const char *fmt, const Args&... args) {
		if (level < m_level && ! _is_raw_level(level))
			return;
		std::lock_guard<std::mutex> lock(m_lock);
		assert(out.good());
		out << m_prompts[level] << FormatArgs(fmt, args...) << std::endl;
	}
	
	inline bool _valid_level(int level) const {
		return (level >= _MIN_LEVEL && level <= _MAX_LEVEL);
	}
	
	inline bool _is_raw_level(int level) const {
		return (level == RAW_OUT_L || level == RAW_ERR_L);
	}
	
private:
	int m_level;
	std::ostream *m_out, *m_err;
	std::mutex m_lock;
	std::map<int, std::string> m_prompts = {
								{DEBUG_L, "Debug: "},
								{INFO_L, "Info: "},
								{STEP_L, "--> "},
								{WARNING_L, "Warning: "},
								{ERROR_L, "ERROR: "},
								{FATAL_L, "FATAL: "},
								{RAW_OUT_L, ""},
								{RAW_ERR_L, ""},
								{RAW_FATAL_L, ""} };
								
	DISALLOW_COPY_AND_ASSIGN(Log);
};



#endif /* JMIN_UTILS_LOG_H */
