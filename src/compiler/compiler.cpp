#include "compiler/compiler.h"
#include "utils/glog.h"
#include <cassert>
#include <sstream>
#include <iostream>
#include <regex>
#include <set>

using namespace jmin;

static std::vector<std::string> SplitLines(std::string const& Contents) {
  std::vector<std::string> Lines;
  std::stringstream SS(Contents);
  std::string Line;

  while (std::getline(SS, Line, '\n'))
    Lines.push_back(Line);

  return Lines;
}

namespace jmin { namespace detail {

static unsigned CalculateNumParsedArgs(std::string const& Msg) {
  int Max = -1;
  std::set<int> Seen;
  std::regex ArgMatcher("%([0-9]+)");
  auto begin = std::sregex_iterator(Msg.begin(), Msg.end(), ArgMatcher);
  auto end = std::sregex_iterator();
  for (auto It = begin; It != end; ++It) {
    std::smatch M = *It;
    int Cur = std::stoi(M[1]);
    Seen.insert(Cur);
    Max = std::max(Cur, Max);
  }
  assert((Max == -1 && Seen.size() == 0) ||
             (Max != -1 && (Max + 1 == (int)Seen.size())));
  return Max + 1;
}

static std::string FormatDiagnostic(PartialDiag const& PD) {
  assert(PD.Args.size() == PD.NumParsedArgs);
  std::string MsgReplaced;
  MsgReplaced.reserve(PD.Msg.size());
  if (PD.Args.size() == 0)
    MsgReplaced = PD.Msg;
  for (unsigned I=0; I < PD.Args.size(); ++I) {
    std::regex ArgRE("%" + std::to_string(I));
    std::regex_replace(
        std::back_inserter(MsgReplaced),
        PD.Msg.begin(), PD.Msg.end(),
        ArgRE, PD.Args.at(I));
  }
  std::string Result = PD.Prefix + " " + MsgReplaced;
  if (!PD.Line.empty())
      Result += "\n" + PD.Line;
  return Result + "\n";
}

PartialDiag::PartialDiag(std::string Prefix, std::string Msg, std::string Line)
    : Prefix(Prefix), Msg(Msg), Line(Line) {
  NumParsedArgs = CalculateNumParsedArgs(Msg);
}

PartialDiag::~PartialDiag() {
  std::string Result = FormatDiagnostic(*this);
  std::cerr << Result << std::endl;
}



}}

Context::Context(Options const &xOpts, std::string File)
    : Opts(xOpts), Symbols(new SymbolTable()), FileLines(SplitLines(File)) {}


detail::PartialDiag Context::Diag(DiagKind K, std::string Msg, int Loc) const {

  assert(Loc == -1 || Loc < (int)FileLines.size());
  assert(Loc != 0);
  std::string Line = Loc == -1 ? "" : FileLines[Loc-1];
  std::string Prefix = Opts.InputFile + ":";
  if (Loc != -1)
    Prefix += std::to_string(Loc) + ":";
  switch (K) {
  case Error:
    ++NumErrors;
    Prefix += " error: ";
    break;
  case Warning:
    Prefix += " warning: ";
    break;
  case Note:
    Prefix += " note: ";
  }
  return {Prefix, Msg, Line};
}

