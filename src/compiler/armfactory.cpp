/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "compiler/armfactory.h"
#include <string>
#include <sstream>
#include <iostream>

#define TAB "\t"
#define TABSPACE4 "\t\t\t"

#define FMT1(i, x)		TAB << i << TABSPACE4 << x << std::endl;
#define FMT2(i, x, y)  	TAB << i << TABSPACE4 << x << ", " << y << std::endl
#define FMT3(i, d, x, y) TAB << i << TABSPACE4 << d << ", " << x << ", " << y << std::endl 

/* initialize static vars */
int ArmFactory::label_num_ = 0;
std::string ArmFactory::generic_label_prefix_ = std::string("L");
std::string ArmFactory::string_label_prefix_ = std::string("LS");

std::string ArmFactory::RTS_STRING = R"(
.set SYS_READ, 0x06
.set SYS_WRITEC, 0x03
.set SYS_WRITE0, 0x04
.set SYS_EXCEPT, 0x18
.set SVC_INT, 0x123456
.set ADP_EXIT, 0x20026

@stay in constant loop
Lhalt:
	mov		r0, #SYS_EXCEPT
	ldr		r1, =ADP_EXIT
	svc		#SVC_INT
.ltorg
.align 4

@get char from console
@@@ I AM NOT WORKING -- Possibly QEMU flags.
@@@ Zero is always returned. 
Lgetchar:
	stmfd 	sp!, {lr}
	sub		sp, #20
	mov		r0, #0
	str		r0, [sp, #0]
	add		r0, sp, #16
	str		r0, [sp, #4]
	mov		r0, #1
	str		r0, [sp, #8]
	mov		r0, #0
	str		r0, [sp, #16]
	str		r0, [sp, #12]
	mov		r0, #SYS_READ
	mov		r1, sp
	svc		#SVC_INT
	ldr		r0, [sp, #16]
	add		sp, #20
	ldmfd	sp!, {pc}
@@ END GETCHAR
		
@print a boolean
Lprintb:
	stmfd	sp!, {lr}
	LDR		r1, =LStrue
	cmp		r0, #0
	LDREQ	r1, =LSfalse
	mov		r0, #SYS_WRITE0
	svc		#SVC_INT
	ldmfd	sp!, {pc}
.ltorg
LStrue:
	.asciz "true"
LSfalse:
	.asciz "false"
.align 4
	
	
@print a character
Lprintc:
	stmfd	sp!, {lr}
	sub		sp, #4
	str		r0, [sp]
	mov		r1, sp		
	mov		r0, #SYS_WRITEC
	svc		#SVC_INT
	add		sp, #4
	ldmfd	sp!, {pc}
@@@ END Lprintc	

@printi print an integer
@ if the number is the minimum integer value then NEG won't work. so cheat.
Lprinti:
	STMFD	sp!, {lr}
	CMP		r0, #0
	BGE		Lprinti_call
	NEG		r0, r0
	@If it is still < 0; then cheat and print special case
	CMP		r0, #0
	BLE		Lprinti_cheat 
	PUSH	{r0}
	MOV		r0, #'-'
	BL		Lprintc
	POP		{r0}
Lprinti_call:
	BL		Lprinti_rec
	B		Lprinti_end
Lprinti_cheat:
	ADRL	r0, Lcheat_str
	BL		Lprints
	B		Lprinti_end
Lprinti_end:
	MOV		r0, #0
	LDMFD	sp!, {pc}
.ltorg
Lcheat_str:
.asciz "-2147483648"
.align 4
@@@ END lprinti

@Lprinti_rec is a helper for Lprinti
Lprinti_rec:
	STMFD	sp!, {lr}
	PUSH		{r0}
	MOV		r1, #10
	SDIV		r0, r0, r1
	CMP		r0, #0
	BEQ		Lprinti_rec_print
	BL		Lprinti_rec
Lprinti_rec_print:
	POP		{r0}
	MOV		r1, #10
	BL		Lmod
	ADD		r0, #'0'
	BL		Lprintc
	LDMFD   sp!, {pc}
.ltorg
.align 4
@@@ END lprinti_rec

@Lprints
Lprints:
	stmfd	sp!, {fp,lr}
	mov		r1, r0
	mov		r0,	#SYS_WRITE0
	svc		#SVC_INT
	ldmfd	sp!, {fp,pc}
@@@ END Lprints

@mod r0 % r1	
Lmod:
	STMFD	sp!, {fp,lr}
	CMP		r1, #0
	BEQ		Ldivbyzero	
	sdiv		r2, r0, r1
	mul		r2, r1, r2
	sub		r0, r0, r2
	ldmfd	sp!, {fp,pc}
@@@ END Lmod

@Ldivbyzero
Ldivbyzero:
	STMFD	sp!, {lr}
	ADRL		r0, LSdivzero
	BL		Lprints
	B		Lhalt
	LDMFD	sp!, {pc}
.ltorg
LSdivzero:
	.asciz "Error: Division by zero\n"
.align 4

@Lnewl
Lnewl:
	stmfd 	sp!, {fp,lr}
	mov		r0, #'\n'
	bl		Lprintc
	ldmfd	sp!, {fp,pc}
)";

std::string ArmFactory::_IntToString(int x) 
{
	std::stringstream ss;
	ss << x;
	return ss.str();
}

std::string ArmFactory::Prologue(std::string main_label)
{
	std::stringstream ss;
	ss << "@Prologue and Epilogue shamelessly stolen with permission from the reference compiler" << std::endl;
	ss << ".global _start" << std::endl;
	ss << "_start:" << std::endl;
	ss << FMT2("ADRL", "sp", "theend");
	ss << FMT2("MOV", "r0", "#1");
	ss << FMT2("LSL", "r0", "#25");
	ss << FMT2("ADD", "sp", "r0");
	ss << FMT2("MOV", "fp", "sp");
	ss << FMT2("ADRL", "ip", "globals");
	ss << FMT1("bl", main_label);
	ss << FMT1("b", "Lhalt");
	return ss.str();
}

std::string ArmFactory::Epilogue()
{
	std::stringstream ss;
	ss << "theend = ." << std::endl;
	return ss.str();
}

std::string ArmFactory::FunctionPrologue(int stack_size)
{
	std::stringstream ss;
	ss << FMT2("STMFD", "sp!", "{fp,lr}");
	ss << FMT2("SUB", "sp", GetConstString(_IntToString(stack_size)));
	ss << FMT2("MOV", "fp", "sp");
	return ss.str();
}

std::string ArmFactory::FunctionEpilogue(int stack_size)
{
	std::stringstream ss;
	ss << FMT2("MOV", "sp", "fp");
	ss << FMT2("ADD", "sp", GetConstString(_IntToString(stack_size)));
	ss << FMT2("LDMFD", "sp!", "{fp,pc}");
	return ss.str();
}

std::string ArmFactory::RunTimeSystem()
{
	return ArmFactory::RTS_STRING;
}

/* label functions */
int ArmFactory::_GetNextLabelNum()
{
	return label_num_++;
}

std::string ArmFactory::GetLabel()
{
	std::stringstream ss;
	ss << generic_label_prefix_ << _GetNextLabelNum();
	return ss.str();
}

std::string ArmFactory::GetStringLabel()
{
	std::stringstream ss;
	ss << string_label_prefix_ << _GetNextLabelNum();
	return ss.str();
}

/* string formatting functions */

std::string ArmFactory::GetLitPoolString(std::string expr)
{
	std::string fmt = std::string("=");
	fmt += expr;
	return fmt;
}

std::string ArmFactory::GetConstString(std::string expr)
{
	return "#"+expr;
}

std::string ArmFactory::FormatRegisterOffset(std::string reg, int offset)
{
	std::stringstream ss;
	ss << "["<<reg<<", #"<<offset<<"]";
	return ss.str();
}

/* PUSH assumes fully desending stack */
std::string ArmFactory::Push(std::string reg)
{
	std::stringstream ss;
	ss << FMT1("PUSH", "{"+reg+"}");
	return ss.str();
}

/* POP assumes fully desending stack */
std::string ArmFactory::Pop(std::string reg)
{
	std::stringstream ss;
	ss << FMT1("POP", "{"+reg+"}");
	return ss.str();
}

std::string ArmFactory::Mov(std::string reg, std::string other)
{
	std::stringstream ss;
	ss << FMT2("MOV", reg, other);
	return ss.str();
}

std::string ArmFactory::Add(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT3("ADD", dest, reg1, reg2);
	return ss.str();
}

std::string ArmFactory::Sub(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT3("SUB", dest, reg1, reg2);
	return ss.str();
}

std::string ArmFactory::SMul(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT3("MUL", dest, reg1, reg2);
	return ss.str();
}

std::string ArmFactory::SDiv(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	/* throw in some RuntimeChecking 
	 * I noticed the ref compiler seemed to insert the print
	 * and the B Halt in locally in the method, I perfer
	 * a RTE centric aproach. Give it and error method, and always
	 * go there. */
	ss << FMT2("CMP", reg2, "#0");
	ss << FMT1("BEQ", "Ldivbyzero");
	ss << FMT3("SDIV", dest, reg1, reg2);
	return ss.str();
}

std::string ArmFactory::Mod(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	/* Call the other method to insert divbyzero check */
	//ss << FMT3("SDIV", dest, reg1, reg2);
	ss << ArmFactory::SDiv(dest, reg1, reg2);
	ss << FMT3("MUL", dest, reg2, dest);
	ss << FMT3("SUB", dest, reg1, dest);
	return ss.str();
}

std::string ArmFactory::Negate(std::string dest, std::string reg1)
{
	std::stringstream ss;
	ss << FMT2("NEG", dest, reg1);
	return ss.str();	
}

std::string ArmFactory::And(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT3("AND", dest, reg1, reg2);
	return ss.str();
}

std::string ArmFactory::Or(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT3("OR", dest, reg1, reg2);
	return ss.str();
}

/* the following comparison operations might be buggy */
std::string ArmFactory::GreaterEqual(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	/* default dest to false */
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	/* if greater/equal dest = true */
	ss << FMT2("MOVGE", dest, "#1");
	return ss.str();
}
std::string ArmFactory::Greater(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	ss << FMT2("MOVGT", dest, "#1");
	return ss.str();
}

std::string ArmFactory::LessEqual(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	ss << FMT2("MOVLE", dest, "#1");
	return ss.str();
}

std::string ArmFactory::Less(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	ss << FMT2("MOVLT", dest, "#1");
	return ss.str();
}

std::string ArmFactory::Equal(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	ss << FMT2("MOVEQ", dest, "#1");
	return ss.str();
}

std::string ArmFactory::NotEqual(std::string dest, std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("MOV", dest, "#0");
	ss << FMT2("CMP", reg1, reg2);
	ss << FMT2("MOVNE", dest, "#1");
	return ss.str();
}

std::string ArmFactory::StoreL(std::string dest, std::string reg1)
{
	std::stringstream ss;
	ss << FMT2("STR", dest, reg1);
	return ss.str();
}

std::string ArmFactory::Load(std::string dest, std::string reg1)
{
	std::stringstream ss;
	ss << FMT2("LDR", dest, reg1);
	return ss.str();
}

std::string ArmFactory::AddressLong(std::string dest, std::string reg1)
{
	std::stringstream ss;
	ss << FMT2("ADRL", dest, reg1);
	return ss.str();
}

std::string ArmFactory::Compare(std::string reg1, std::string reg2)
{
	std::stringstream ss;
	ss << FMT2("CMP", reg1, reg2);
	return ss.str();
}

std::string ArmFactory::BranchAlways(std::string label)
{
	std::stringstream ss;
	ss << FMT1("B", label);
	return ss.str();
}

std::string ArmFactory::BranchLink(std::string label)
{
	std::stringstream ss;
	ss << FMT1("BL", label);
	return ss.str();
}

std::string ArmFactory::BranchConditional(std::string condition, std::string label)
{
	std::stringstream ss;
	ss << FMT1("B" + condition, label);
	return ss.str();
}

std::string ArmFactory::ASCIZString(std::string s)
{
	std::stringstream ss;
	ss << ".asciz " << s << std::endl;
	return ss.str();
}
