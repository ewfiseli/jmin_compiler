#ifndef JMIN_COMPILER_SEMA_H
#define JMIN_COMPILER_SEMA_H

#include "AST/AST.h"
#include "compiler/compiler.h"

namespace jmin {

using namespace AST;

struct Sema {
  Sema(Context &Ctx) : Ctx(Ctx) {}

  Type *ActOnType(int Loc, std::string Name, int BuiltinTypeID);
  Literal *ActOnLiteral(int Loc, std::string Value, int LiteralID);
  Ident *ActOnIdentifier(int Loc, std::string Name);
  UnaryExpression *ActOnUnaryExpr(int Loc, Node *E, int UnaryOpID);
  NullStmt *ActOnNullExpr(int Loc);
  BreakStatement *ActOnBreakStmt(int Loc);
  ReturnStatement *ActOnReturnStmt(int Loc, Node *E = nullptr);
  IfStatement *ActOnIfStmt(int Loc, Node *Expr, Node *Stmts);
  IfElseStatement *ActOnIfElseStmt(int Loc, Node *Expr, Node *IfStmt,
                                   Node *ElseStmt);
  WhileStatement *ActOnWhileStmt(int Loc, Node *Expr, Node *Body);

  detail::PartialDiag Diag(DiagKind K, std::string Msg, int Loc) const {
    return Ctx.Diag(K, Msg, Loc);
  }

  detail::PartialDiag Diag(detail::DiagID const &ID, int Loc) const {
    return Ctx.Diag(ID.Kind, ID.Msg, Loc);
  }

  Context &Ctx;
};
} // namespace jmin

#endif // JMIN_COMPILER_SEMA_H
