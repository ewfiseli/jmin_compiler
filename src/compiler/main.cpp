/*
 * Copyright (C) 2013  Eric Fiselier
 *
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AST/AST.h"
#include "compiler/compiler.h"
#include "compiler/sema.h"
#include "compiler/symboltable.h"
#include "utils/common.h"
#include "utils/glog.h"
#include "visitors/codeelimvisitor.h"
#include "visitors/codegenerationvisitor.h"
#include "visitors/constfoldingvisitor.h"
#include "visitors/functionsemanticvisitor.h"
#include "visitors/globalsemanticvisitor.h"
#include "visitors/printvisitor.h"
#include "visitors/typesemanticvisitor.h"

#include <fstream>
#include <iostream>
#include <string>

/* the parser should assign ast_root_ to be the root node
 * a small hack because the c++ interface for flex/bison and cmake is a
 * pain to use together */
AST::RootNode *ast_root_;

extern FILE *yyin;
extern int yyparse();

using namespace jmin;


/* run the print visitor on the root node to print the AST */
void PrintTree(AST::RootNode *root);
/* This runs 3 semantic checking visitors in a very specific order
 * each visitor assumes that the AST has passed on the last visitor */
void CheckASTSemantics(AST::RootNode *root_node, Context &Ctx);

/* This generates the code and returns it as a string.
 * keeping the code in a string not a file allows for
 * ungraceful exiting (via Log::Error or Log::Fatal) to not leave the output
 * file in a weird state */
std::string GenerateCode(AST::RootNode *root, Context &Ctx);

/* pre-compile and post-compile optimizations should be run from here */
void RunASTOptimizations(AST::RootNode *root_node);
void RunAssemblyOptimizations(std::string filename);

static void PrintUsageAndExit() {
  std::cout << "jmin <input-file> [--output=<output-file>]"
            << "    [--no-optimize] [--no-optimize-ast] [--no-library] [--no-prologue]"
            << std::endl;
  std::exit(1);
}

static bool ParseStringFlag(const char *FlagName, std::string &Dest,
                            std::string const &Arg) {
  if (Arg.compare(0, strlen(FlagName), FlagName) != 0)
    return false;
  Dest = Arg.substr(strlen(FlagName));
  return true;
}

static bool ParseBoolFlag(const char *FlagName, bool &Dest, bool Value,
                          std::string const &Arg) {
  if (Arg != FlagName)
    return false;
  Dest = Value;
  return true;
}

static bool ParseArguments(int &argc, char **argv, Options &Opts) {
  if (argc < 2)
    PrintUsageAndExit();
  Opts.InputFile = argv[1];
  for (int I = 2; I < argc; ++I) {
    std::string Arg = argv[I];
    if (ParseStringFlag("--output=", Opts.OutputFile, Arg) ||
        ParseBoolFlag("--no-optimize", Opts.RunOptimizations, false, Arg) ||
        ParseBoolFlag("--no-optimize-ast", Opts.RunASTOptimizations, false, Arg) ||
        ParseBoolFlag("--no-library", Opts.IncludeLibrary, false, Arg) ||
        ParseBoolFlag("--no-prologue", Opts.IncludePrologue, false, Arg) ||
        ParseBoolFlag("--ast-dump", Opts.DumpAST, true, Arg) ||
        ParseBoolFlag("--syntax-only", Opts.SyntaxOnly, true, Arg)) {
      for (int J = I; J < (argc - 1); ++J)
        argv[J] = argv[J + 1];
      --argc;
      --I;
    } else
      PrintUsageAndExit();
  }
  return false;
}

int main(int argc, char *argv[]) {
  Options Opts;
  if (ParseArguments(argc, argv, Opts))
    return 1;

  std::ifstream infile(Opts.InputFile);
  if (!infile.good())
    GError("file \'%s\' not found", Opts.InputFile.c_str());
  std::string Contents((std::istreambuf_iterator<char>(infile)),
                       std::istreambuf_iterator<char>());
  infile.close();
  Context Ctx(Opts, Contents);
  Sema S(Ctx);

  yyin = fopen(Opts.InputFile.c_str(), "r");
  assert(yyin != 0);

  if (yyparse() != 0)
    FATAL_ERROR();

  /* create symbol table and load it while
   * checking AST semantics */
  delete Ctx.Symbols;
  Ctx.Symbols = new SymbolTable();
  if (Opts.IncludeLibrary)
    AddRuntimeLibrary(Ctx.Symbols);
  CheckASTSemantics(ast_root_, Ctx);

  if (Opts.SyntaxOnly || Ctx.NumErrors)
    return Ctx.NumErrors ? 1 : 0;

  /* run AST optimizations */
  PrintTree(ast_root_);
  if (Opts.RunASTOptimizations)
    RunASTOptimizations(ast_root_);
  /* this is not nice, but after eliminating unreachable code
   * we need to rebuild the symbol table to remove symbols declared in removed
   * code.*/
  delete Ctx.Symbols;
  Ctx.Symbols = new SymbolTable();
  if (Opts.IncludeLibrary)
    AddRuntimeLibrary(Ctx.Symbols);


  if (Opts.DumpAST) {
    PrintVisitor v;
    v.Visit(ast_root_);
    return 0;
  }

  /* generate code and write it to the output file */
  std::string code = GenerateCode(ast_root_, Ctx);
  {
    std::ostream *Out = nullptr;
    std::ofstream file;
    if (Opts.OutputFile == "-")
      Out = &std::cout;
    else {
      file.open(Opts.OutputFile.c_str());
      assert(file.good());
      Out = &file;
    }
    *Out << code;
  }

  if (Opts.RunOptimizations)
    RunAssemblyOptimizations(Opts.OutputFile);

  return 0;
}

/* Run all semantic checks on an AST rooted at root_node
 * The visitors will exit if an error is found */
void CheckASTSemantics(AST::RootNode *root_node, Context &Ctx) {
  assert(root_node);
  assert(Ctx.Symbols);

  /* load global symbols into table, check main declarations, ect... */
  GlobalSemanticVisitor global_visitor(Ctx);
  root_node->AcceptVisit(&global_visitor);

  /* check functions for general errors like return type, undeclared or
   * improperly declared variables, ect... It also checks break statements.
   */
  FunctionSemanticVisitor function_visitor(Ctx);
  root_node->AcceptVisit(&function_visitor);

  /* this visitor checks everything related to types, such as assignments,
   * return types, loop/if/else condition types, ect...
   */
  TypeSemanticVisitor type_visitor(Ctx);
  root_node->AcceptVisit(&type_visitor);

  /* All done! no errors! */
}

/* Pre-compile (AST) optimizations go here
 * Currently only Constant folding is planned */
void RunASTOptimizations(AST::RootNode *root) {
  /* fold constants in the AST */
  ConstFoldingVisitor const_visitor;
  root->AcceptVisit(&const_visitor);
  /* remove unreachable code */
  CodeElimVisitor codeelim_visitor;
  root->AcceptVisit(&codeelim_visitor);
}

void RunAssemblyOptimizations(std::string filename) {
  UNUSED(filename);
  /* todo peephole, probably not before submission */
}

std::string GenerateCode(AST::RootNode *root, Context &Ctx) {
  CodeGenerationVisitor code_visitor(Ctx);
  root->AcceptVisit(&code_visitor);
  return code_visitor.ToString();
}

void PrintTree(AST::RootNode *root) {
  PrintVisitor print_visitor;
  root->AcceptVisit(&print_visitor);
}
