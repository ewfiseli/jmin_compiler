%{
/* 
	Author: Eric Fiselier
	Uses flex 2.5.35
	creates a scanner using flex 
*/

#include "compiler/parser.h"
#include "utils/common.h"


%}
		
%option yylineno
%option noyywrap
%option nounput 

L				[a-zA-Z_]

%%

"void"					{ return VOID; }
"if"						{ return IF; }
"else"					{ return ELSE; }
"while"					{ return WHILE; }
"break"					{ return BREAK; }
"return"					{ return RETURN; }

"boolean"				{ return BOOLEAN_TYPE; }
"int"					{ return INT_TYPE; }

"true"					{ return TRUE_L; }
"false"					{ return FALSE_L; }

[_a-zA-Z][_a-zA-Z0-9]*	{ return ID; }

L?\"(\\.|[^\\"])*\"		{ return STRING_L; }
[0-9]+					{ return NUMBER_L; }

"=="						{ return EQ_OP; }
"!="						{ return NE_OP; }
"<="						{ return LE_OP; }
">="						{ return GE_OP; }
"&&"						{ return AND_OP; }
"<"						{ return LT_OP; }
">"						{ return GT_OP; }
"||"						{ return OR_OP; }
"="						{ return ASSIGN_OP; }
"+"						{ return PLUS_OP; }
"-"						{ return MINUS_OP; }
"*"						{ return MUL_OP; }
"/"						{ return DIV_OP; }
"%"						{ return PERCENT_OP; }
"!"						{ return EXCLAM_OP; }
"("						{ return LPAREN_CC; }
")"						{ return RPAREN_CC; }
"{"						{ return LBRACE_CC; }
"}"						{ return RBRACE_CC; }
","						{ return COMA_CC; }
";"						{ return SEMICOLON_CC; }

[ \n\t\r]+				/* WHITESPACE!! */
[/]{2}[^\n]*			/* COMMENT */
.						{ return INVALID_CHAR; }

%%

