#ifndef JMIN_COMPILER_COMPILER_H
#define JMIN_COMPILER_COMPILER_H

#include "symboltable.h"
#include <string>
#include <vector>
#include <sstream>
#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

namespace jmin {

struct Options {
  std::string InputFile;
  std::string OutputFile = "a.out";
  bool RunOptimizations = true;
  bool RunASTOptimizations = true;
  bool IncludeLibrary = true;
  bool IncludePrologue = true;
  bool DumpAST = false;
  bool SyntaxOnly = false;
};

enum DiagKind {
  Error,
  Warning,
  Note
};

constexpr int InvalidLoc = -1;

namespace detail {
  struct DiagID {
    DiagKind Kind;
    std::string Msg;
  };
  struct PartialDiag {
    std::string Prefix;
    std::string Msg;
    std::string Line;
    std::vector<std::string> Args;
    unsigned NumParsedArgs;

    template <class Tp>
    void appendArg(Tp const& arg) {
      assert(Args.size() < NumParsedArgs);
      std::stringstream ss;
      ss << arg;
      Args.push_back(ss.str());
    }

    PartialDiag(std::string Prefix, std::string Msg, std::string Line);
    ~PartialDiag();

    PartialDiag(PartialDiag const&) = delete;
    PartialDiag& operator=(PartialDiag const&) = delete;
    PartialDiag(PartialDiag&&);
  };

  template <class Tp>
  PartialDiag&& operator<<(PartialDiag&& D, Tp const& Arg) {
    D.appendArg(Arg);
    return std::move(D);
  }
} // namespace Detail

namespace diag {
using detail::DiagID;

inline DiagID err_variable_redefined{Error, "variable '%0' redefined"};
inline DiagID note_func_declared_here{Note, "function declared here"};
inline DiagID note_declared_here{Note, "previously declared here:"};

}

struct Context {
  Context(Options const& Opts, std::string FileContents);
  Context(Context const&) = delete;
  Context& operator=(Context const&) = delete;

  detail::PartialDiag Diag(DiagKind K, std::string Msg, int Loc)  const;
  detail::PartialDiag Diag(detail::DiagID const& ID, int Loc)  const {
    return Diag(ID.Kind, ID.Msg, Loc);
  }

public:

  Options const& Opts;
  SymbolTable *Symbols;

  mutable unsigned NumErrors = 0;
private:
  std::vector<std::string> FileLines;
};

} // namespace jmin

#endif
