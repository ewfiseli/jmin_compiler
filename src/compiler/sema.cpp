#include "compiler/sema.h"
#include "compiler/parser.h"

using namespace jmin;

Type *Sema::ActOnType(int Loc, std::string Name, int BuiltinTypeID) {
  return new Type(Name, BuiltinTypeID, Loc);
}

Literal *Sema::ActOnLiteral(int Loc, std::string Value, int LiteralID) {
  assert(LiteralID == STRING_L || LiteralID == NUMBER_L ||
         LiteralID == TRUE_L || LiteralID == FALSE_L);
  return new Literal(Value, LiteralID, Loc);
}
