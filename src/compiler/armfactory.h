/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#ifndef ARMFACTORY_H
#define ARMFACTORY_H

/* Author: Eric Fiselier
 * The idea of using a "CodeFactory" is an attempt to 
 * de-couple the AST code generator from the language it is generating
 * the ARMFactory is meant to represent a somewhat more
 * generic assembly interface. The end goal is to create a generic assembly
 * interface, in future this factory should be able to be swapped out with
 * an X86 factory or some other assembly language. or even a better ARM factory.
 */
#include <string>

/* Author: Eric Fiselier
 * These functions are meant to represent pseudo assembly 
 * unstructions. They ouput the actual instructions.
 * it is assumed that all strings are well-formatted
 * and are allowed to be used with the instructions.
 * ie I do no checking 
 */

class ArmFactory
{
public:
	/* Output a string of properly formated arm assembly intruction(s) */
	static std::string Prologue(std::string main_label);
	static std::string Epilogue();
	static std::string FunctionPrologue(int stack_size);
	static std::string FunctionEpilogue(int stack_size);
	static std::string RunTimeSystem();
	static std::string Push(std::string reg);
	static std::string Pop(std::string reg);
	static std::string Mov(std::string reg, std::string other);
	static std::string Add(std::string dest, std::string reg1, std::string reg2);
	static std::string Sub(std::string dest, std::string reg1, std::string reg2);
	static std::string SMul(std::string dest, std::string reg1, std::string reg2);
	static std::string SDiv(std::string dest, std::string reg1, std::string reg2);
	static std::string Mod(std::string dest, std::string reg1, std::string reg2);
	static std::string Negate(std::string dest, std::string reg1);
	static std::string And(std::string dest, std::string reg1, std::string reg2);
	static std::string Or(std::string dest, std::string reg1, std::string reg2);
	static std::string GreaterEqual(std::string dest, std::string reg1, std::string reg2);
	static std::string Greater(std::string dest, std::string reg1, std::string reg2);
	static std::string LessEqual(std::string dest, std::string reg1, std::string reg2);
	static std::string Less(std::string dest, std::string reg1, std::string reg2);
	static std::string Equal(std::string dest, std::string reg1, std::string reg2);
	static std::string NotEqual(std::string dest, std::string reg1, std::string reg2);
	static std::string StoreL(std::string dest, std::string reg1);
	static std::string Load(std::string dest, std::string reg1);
	static std::string AddressLong(std::string dest, std::string reg1);
	static std::string Compare(std::string reg1, std::string reg2);
	static std::string BranchAlways(std::string label);
	static std::string BranchLink(std::string label);
	static std::string BranchConditional(std::string condition, std::string label);
	static std::string ASCIZString(std::string s);
	//TODO i'm sure im missing at least one method
	
	/* not pseudo assembly 
	 * 
	 * GetLitPoolString takes a label/integer (something in the lit pool)
	 * and prepends it with "="
	 * 
	 * GetConstString should only take an integer string that is positive
	 * 				  and small (I can't remeber how small for ARM)
	 * 				  it returns the string with "#" prepended
	 */
	 static std::string GetLitPoolString(std::string expr);
	 static std::string GetConstString(std::string expr);
	 static std::string FormatRegisterOffset(std::string reg, int offset);
	
	/* A little hacky; unique label generation is my job.
	 * I don't know why. but it is. possibly because I might
	 * want different labels for different assembly factories? */
	static std::string GetLabel();
	static std::string GetStringLabel();
private:
	static std::string _IntToString(int x);
	static int _GetNextLabelNum();
	static int label_num_;
	static std::string generic_label_prefix_;
	static std::string string_label_prefix_;
	
	/* TODO put RTS in variable here. */
	static std::string RTS_STRING;
};

#endif /* ARMFACTORY_H */