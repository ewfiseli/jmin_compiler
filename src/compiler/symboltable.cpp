/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "compiler/symboltable.h"

#include "AST/AST.h"
#include "utils/glog.h"

using namespace AST;

SymbolTable::SymbolTable() : local_table_name_("")
{
	main_func_ = 0;
	local_var_table_ = 0;
}

SymbolTable::~SymbolTable()
{
	var_table_list_t::iterator it;
	for (it=local_tables_.begin(); it != local_tables_.end(); ++it) 
		if (it->second) delete (it->second);
}

bool SymbolTable::HasFunction(std::string id)
{
	int count = func_table_.count(id);
	assert(count == 0 || count == 1);
	return (count == 1);
}

void SymbolTable::SetFunction(FunctionHeader *header)
{
	if (! header) return;
	
	std::string id_str = header->GetName();
	func_table_[id_str] = new FunctionHeader(*header);
}

FunctionHeader *SymbolTable::GetFunction(std::string id)
{
	if (! HasFunction(id)) return 0;
	
	return func_table_[id];
}

func_table_t &SymbolTable::GetFunctionTable()
{
	return func_table_;
}


bool SymbolTable::HasMain()
{
	return (main_func_ != 0);
}

void SymbolTable::SetMain(MainFunctionDeclarator *main_func)
{
	if (main_func_) delete main_func_;
	main_func_ = new MainFunctionDeclarator(*main_func); 
}

MainFunctionDeclarator *SymbolTable::GetMain()
{
	return main_func_;
}

bool SymbolTable::HasGlobalVar(std::string id)
{
	int count = global_var_table_.count(id);
	assert(count == 0 || count == 1);
	return (count == 1);
}

void SymbolTable::SetGlobalVar(VariableDecl *decl)
{
	if (! decl) return;
	std::string id = decl->GetID()->GetName();
	global_var_table_[id] = new VariableDecl(*decl);
}

VariableDecl *SymbolTable::GetGlobalVar(std::string id)
{
	if (! HasGlobalVar(id)) return 0;
	
	return global_var_table_[id];
}

var_table_t &SymbolTable::GetGlobalTable()
{
	return global_var_table_;
}

bool SymbolTable::HasLocalVar(std::string id)
{
	if (! LocalTableLoaded()) return false;
	var_table_t &local_table = GetLocalTable();
	
	int count = local_table.count(id);
	assert(count == 0 || count == 1);
	return (count == 1);
}

void SymbolTable::SetLocalVar(VariableDecl *decl)
{
	if (! LocalTableLoaded() || !decl) return;
	var_table_t &local_table = GetLocalTable();
	
	std::string id = decl->GetName();
	local_table[id] = new VariableDecl(*decl); 
}

VariableDecl *SymbolTable::GetLocalVar(std::string id)
{
	if (! HasLocalVar(id)) return 0;
	var_table_t &local_table = GetLocalTable();
	
	return local_table[id];
}


bool SymbolTable::HasVar(std::string id)
{
	bool has_global = HasGlobalVar(id);
	
	bool has_local = false;
	if (LocalTableLoaded())
		has_local = HasLocalVar(id);
	
	
	return (has_global || has_local);
}

VariableDecl *SymbolTable::GetVar(std::string id)
{
	if (LocalTableLoaded() && HasLocalVar(id))
		return GetLocalVar(id);
	
	if (HasGlobalVar(id)) 
		return GetGlobalVar(id);
	
	return 0;
}

bool SymbolTable::LocalTableLoaded()
{
	return (local_var_table_ != 0);
}

std::string SymbolTable::LocalTableName()
{
	return local_table_name_;
}

var_table_t &SymbolTable::GetLocalTable()
{
	assert(local_var_table_);
	return (*local_var_table_);
}

void SymbolTable::CreateLocalTable(std::string id)
{
	UnloadLocalTable();
	
	local_var_table_ = new var_table_t;
	local_tables_[id] = local_var_table_;
}

bool  SymbolTable::HasLocalTable(std::string id)
{
	int count = local_tables_.count(id);
	assert(count == 0 || count == 1);
	return (count == 1);
}

bool SymbolTable::LoadLocalTable(std::string id)
{
	UnloadLocalTable();
	if (! HasLocalTable(id)) return false;
	
	local_var_table_ = local_tables_[id];
	local_table_name_ = id;
	return true;
}

void SymbolTable::UnloadLocalTable()
{
	local_var_table_ = 0;
	local_table_name_ = "";
}

var_table_list_t &SymbolTable::GetLocalTables()
{
	return (local_tables_);
}
