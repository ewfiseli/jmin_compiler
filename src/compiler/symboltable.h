/* 
 * Copyright (C) 2013  Eric Fiselier
 * 
 * This file is part of jmin.
 *
 * jmin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jmin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jmin.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <string>
#include <cstring>
#include <map>

namespace AST {
class FunctionHeader;
class VariableDecl;
class MainFunctionDeclarator;
class Ident;
}

using func_table_t = std::map<std::string, AST::FunctionHeader*>;
using var_table_t = std::map<std::string, AST::VariableDecl*>;
using var_table_list_t = std::map<std::string, var_table_t*>;

class SymbolTable
{
public:
	SymbolTable();
	~SymbolTable();
	
	bool HasFunction(std::string id);
	void SetFunction(AST::FunctionHeader *header);
	AST::FunctionHeader *GetFunction(std::string id);
	func_table_t &GetFunctionTable();
	
	bool HasMain();
	void SetMain(AST::MainFunctionDeclarator *main_func);
	AST::MainFunctionDeclarator *GetMain();
	
	bool HasGlobalVar(std::string id);
	void SetGlobalVar(AST::VariableDecl *var);
	AST::VariableDecl *GetGlobalVar(std::string id);
	var_table_t &GetGlobalTable();
	
	bool HasLocalVar(std::string id);
	void SetLocalVar(AST::VariableDecl *var);
	AST::VariableDecl *GetLocalVar(std::string id);
	
	// Searches both local and global
	bool HasVar(std::string id);
	AST::VariableDecl *GetVar(std::string id);
	
	// checks if local table is loaded
	bool LocalTableLoaded();
	std::string LocalTableName();
	// get the local table, bad times if no table loaded
	var_table_t &GetLocalTable();
	void CreateLocalTable(std::string id);
	bool HasLocalTable(std::string id);
	bool LoadLocalTable(std::string id);
	void UnloadLocalTable();
	var_table_list_t &GetLocalTables();
	
private:
	func_table_t func_table_;
	
	var_table_t global_var_table_;
	var_table_t *local_var_table_;
	
	std::string local_table_name_;
	
	var_table_list_t local_tables_;
	AST::MainFunctionDeclarator *main_func_;
};

#endif // SYMBOLTABLE_H